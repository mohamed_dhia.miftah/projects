import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DeviceComponent} from './device/device.component';
import {DeviceService} from './services/device.service';
import {AuthenticationComponent} from './authentication/authentication.component';
import {DeviceViewComponent} from './device-view/device-view.component';
import {RouterModule, Routes} from '@angular/router';
import {AuthenticationService} from './services/authentication.service';
import {SingleDeviceComponent} from './single-device/single-device.component';
import {NotFound404Component} from './not-found404/not-found404.component';
import {AuthenticationGuard} from './services/authent-guard.service';
import {UpdateDeviceComponent} from './update-device/update-device.component';
import {UserListComponent} from './user-list/user-list.component';
import {UserAddComponent} from './user-add/user-add.component';
import {HttpClientModule} from "@angular/common/http";

const appRoutes: Routes = [
    {path: 'devices', canActivate: [AuthenticationGuard], component: DeviceViewComponent},
    {path: 'devices/:id', canActivate: [AuthenticationGuard], component: SingleDeviceComponent},
    {path: 'update', canActivate: [AuthenticationGuard], component: UpdateDeviceComponent},
    {path: 'users', canActivate: [AuthenticationGuard], component: UserListComponent},
    {path: 'addUser', canActivate: [AuthenticationGuard], component: UserAddComponent},
    {path: 'auth', component: AuthenticationComponent},
    {path: '', component: AppComponent},
    {path: 'not-found', component: NotFound404Component},
    /* redirectTo must be in the end to know all the path before */
    {path: '**', redirectTo: '/not-found'},
]

@NgModule({
    declarations: [
        AppComponent,
        DeviceComponent,
        AuthenticationComponent,
        DeviceViewComponent,
        SingleDeviceComponent,
        NotFound404Component,
        UpdateDeviceComponent,
        UserListComponent,
        UserAddComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes)
    ],
    providers: [DeviceService, AuthenticationService, AuthenticationGuard],
    bootstrap: [AppComponent]
})
export class AppModule {
}
