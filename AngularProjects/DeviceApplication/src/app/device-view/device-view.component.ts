import { Component, OnInit } from '@angular/core';
import { DeviceService } from '../services/device.service';
import {Subscription} from "rxjs";

@Component({
    selector: 'app-device-view',
    templateUrl: './device-view.component.html',
    styleUrls: ['./device-view.component.scss'],
})
export class DeviceViewComponent implements OnInit {
    isAuthentified = false;

    lastUpdate = new Promise((resolve, reject) => {
        const date = new Date();
        setTimeout(() => {
            resolve(date);
        }, 1000);
    });

    listDevices: any[] = [];
    deviceSubscription : Subscription = new Subscription();

    constructor(private deviceService: DeviceService) {
        setTimeout(() => {
            this.isAuthentified = true;
        }, 2000);
    }

    ngOnInit() {
        // this.listDevices = this.deviceService.listDevices;
        this.deviceSubscription = this.deviceService.deviceSubject.subscribe(
            (listDevicesSubscription : any[]) => {
                this.listDevices = listDevicesSubscription;
            }
        )
        this.deviceService.emitDeviceSubject();
    }

    switchOnAllDevices() {
        this.deviceService.switchOnAll();
    }

    switchOffAllDevices() {
        this.deviceService.switchOffAll();
    }

    onSaveAllDevices(){
        this.deviceService.saveDeviceToDataBase();
    }

    onGetAllDevices(){
        this.deviceService.getDevicesFromDataBase();
    }
}
