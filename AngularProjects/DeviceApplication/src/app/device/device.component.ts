import {Component, Input, OnInit} from '@angular/core';
import {DeviceService} from '../services/device.service';

@Component({
    selector: 'app-device',
    templateUrl: './device.component.html',
    styleUrls: ['./device.component.scss']
})
export class DeviceComponent implements OnInit {

    /* Begin 1 */
    // deviceName = "ordinateur portable ";
    // deviceStatus = "eteint";
    /* End 1 */

    /* Begin 2 */
    deviceStatusOn: string = "allume";
    deviceStatusOff: string = "eteint";

    @Input() deviceId: number = 1;
    @Input() deviceName: string = "";
    @Input() deviceStatus: string = "";
    @Input() indexOfDevice: number = 1;

    constructor(private deviceService: DeviceService) {
    }

    ngOnInit(): void {
    }

    getDeviceStatus() {
        return this.deviceStatus;
    }

    getColor() {
        if (this.deviceStatus === this.deviceStatusOn) {
            return 'green';
        } else if (this.deviceStatus === this.deviceStatusOff) {
            return 'red';
        } else {
            return 'black';
        }
    }

    onSwitchOnDevice() {
        console.log("index", this.indexOfDevice);
        this.deviceService.switchOnOne(this.indexOfDevice);
    }

    onSwitchOffDevice() {
        // console.log("index",this.indexOfDevice);
        this.deviceService.switchOffOne(this.indexOfDevice);
    }
}
