import {Subject} from "rxjs";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class DeviceService {

    deviceStatusOn: string = "allume";
    deviceStatusOff: string = "eteint";

    deviceSubject = new Subject<any[]>();

    private listDevices = [
        {id: 0, name: "", status: ''},
        // {id: 1, name: "PC1", status: this.deviceStatusOff},
        // {id: 2, name: "PC2", status: this.deviceStatusOn},
        // {id: 3, name: "PC3", status: this.deviceStatusOff},
    ];

    constructor(private httpClient: HttpClient) {
    }

    emitDeviceSubject() {
        this.deviceSubject.next(this.listDevices.slice());
    }

    getDeviceById(idInput: number) {
        const device = this.listDevices.find((deviceObject) => {
            return deviceObject.id === idInput;
        });
        return device;
    }

    switchOnAll() {
        for (let device of this.listDevices) {
            device.status = this.deviceStatusOn;
        }
        this.emitDeviceSubject();
    }

    switchOffAll() {
        for (let device of this.listDevices) {
            device.status = this.deviceStatusOff;
        }
        this.emitDeviceSubject();
    }

    switchOnOne(indexOfDevice: number) {
        this.listDevices[indexOfDevice].status = this.deviceStatusOn;
        this.emitDeviceSubject();
    }

    switchOffOne(indexOfDevice: number) {
        this.listDevices[indexOfDevice].status = this.deviceStatusOff;
        this.emitDeviceSubject();
    }

    addDevice(deviceName: string, deviceStatus: string) {
        const deviceObject = {
            id: 0,
            name: '',
            status: '',
        };

        deviceObject.name = deviceName;
        deviceObject.status = deviceStatus;
        deviceObject.id = this.listDevices[(this.listDevices.length - 1)].id + 1;

        this.listDevices.push(deviceObject);
        this.emitDeviceSubject();
    }

    saveDeviceToDataBase() {
        this.httpClient.put('https://deviceapplicationfirebase-default-rtdb.europe-west1.firebasedatabase.app/devices.json', this.listDevices)
            .subscribe(
                () => {
                    console.log('Saving succeeded !');
                },
                (error) => {
                    console.log('Error saving ! ' + error);
                })
    }

    getDevicesFromDataBase() {
        this.httpClient
            .get<any>('https://deviceapplicationfirebase-default-rtdb.europe-west1.firebasedatabase.app/devices.json')
            .subscribe(
                (response) => {
                    this.listDevices = response;
                    this.emitDeviceSubject();
                },
                (error) => {
                    console.log('Error response ! ' + error);
                }
            );
    }
}
