import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DeviceService } from '../services/device.service';

@Component({
    selector: 'app-single-device',
    templateUrl: './single-device.component.html',
    styleUrls: ['./single-device.component.scss']
})
export class SingleDeviceComponent implements OnInit {

    deviceName: string | undefined = "deviceName";
    deviceStatus: string | undefined = "deviceStatus";

    constructor(private deviceService: DeviceService, private route: ActivatedRoute) { }

    ngOnInit(): void {
        const idInputUrl = this.route.snapshot.params['id'];
        this.deviceName = this.deviceService.getDeviceById(+idInputUrl)?.name;
        this.deviceStatus = this.deviceService.getDeviceById(+idInputUrl)?.status;
    }

}
