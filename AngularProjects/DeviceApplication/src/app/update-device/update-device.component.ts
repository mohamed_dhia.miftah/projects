import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {DeviceService} from "../services/device.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-update-device',
    templateUrl: './update-device.component.html',
    styleUrls: ['./update-device.component.scss']
})
export class UpdateDeviceComponent implements OnInit {

    constructor(private deviceService: DeviceService, private  router : Router) {
    }

    defaultDeviceStatus = this.deviceService.deviceStatusOff;
    deviceStatusOff = this.deviceService.deviceStatusOff;
    deviceStatusOn = this.deviceService.deviceStatusOn;

    ngOnInit(): void {
    }

    onSubmitForm(form: NgForm) {
        const deviceName = form.value['deviceName'];
        const deviceStatus = form.value['deviceStatus'];
        this.deviceService.addDevice(deviceName,deviceStatus);
        this.router.navigate(['/devices']);
    }

}
