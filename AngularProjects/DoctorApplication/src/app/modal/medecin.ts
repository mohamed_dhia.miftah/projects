export class Medecin {

    id : number ;
    nom: string;
    prenom: string;
    codePublic : number;
    dateNaissance: string;
    email: string;
    photo : string;

    constructor(){
    }
}
