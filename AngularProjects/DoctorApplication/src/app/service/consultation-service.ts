import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { MedSpecialiste } from 'app/modal/med-specialiste';
// importer le map de rxjs
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Consultation } from 'app/modal/consultation';


@Injectable()
export class ConsultationService {

  private consultation:Consultation;
  
  private baseUrl:string='http://localhost:8081/cabinet';
  private headers= new Headers({'Content-Type':'application/json'});
//  private options = new RequestOptions({headers:this.headers});

  constructor(private http: HttpClient) {
  }


  
  getConsultations(): Observable<any> {
    return this.http.get(this.baseUrl+'/getAllConsultation');
  }

  
  saveConsultation(consultation:Consultation):Observable<any>{
    return this.http.post(this.baseUrl+'/addConsultation',consultation);
  }
                
  updateConsultation(id:number,consultation:Consultation):Observable<any>{
    return this.http.put(this.baseUrl+'/updateConsultation/'+id,consultation);
}

  deleteConsultation(id:number){ 
    return this.http.delete(this.baseUrl+'/deleteConsultation/'+id)
    .map((response:Response)=>response.json()) 
    .catch(this.errorHandle);   
  } 
        
    //afficher en cas d'erreur
  errorHandle(error:Response){
    return Observable.throw(error||"Server Error");
  }
    //getters & setters pour l'edit
  setter(consultation:Consultation){
    this.consultation=consultation;
  }
  getter(){
    return this.consultation;
  }   
}
