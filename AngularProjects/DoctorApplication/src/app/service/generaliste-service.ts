import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { MedGeneraliste } from 'app/modal/med-generaliste';

@Injectable()
export class GeneralisteService {

  private medecin:MedGeneraliste;
  
  private baseUrl:string='http://localhost:8081/cabinet';
  private headers= new Headers({'Content-Type':'application/json'});

  constructor(private http: HttpClient) {
  }
  
  getAllMedecinGen(): Observable<any> {
    return this.http.get(this.baseUrl+'/getAllMedecinGeneraliste');
  }

  saveMedecinGen(medecin:MedGeneraliste):Observable<any>{
    return this.http.post(this.baseUrl+'/addMedecinGeneraliste',medecin);
  }
                
  updateMedecinGen(id:number,medecin:MedGeneraliste):Observable<any>{
    return this.http.put(this.baseUrl+'/updateMedecinGeneraliste/'+id,medecin);
}

  deleteMedecinGen(id:number){ 
    return this.http.delete(this.baseUrl+'/deleteMedecinGeneraliste/'+id)
    .map((response:Response)=>response.json()) 
    .catch(this.errorHandle);   
  } 
        
    //afficher en cas d'erreur
  errorHandle(error:Response){
    return Observable.throw(error||"Server Error");
  }
    //getters & setters pour l'edit
  setter(medecin:MedGeneraliste){
    this.medecin=medecin;
  }
  getter(){
    return this.medecin;
  }   
}