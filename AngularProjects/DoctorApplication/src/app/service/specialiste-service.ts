import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { MedSpecialiste } from 'app/modal/med-specialiste';
// importer le map de rxjs
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@Injectable()
export class SpecialisteService {

  private medecin:MedSpecialiste;
  
  private baseUrl:string='http://localhost:8081/cabinet';
  private headers= new Headers({'Content-Type':'application/json'});
//  private options = new RequestOptions({headers:this.headers});

  constructor(private http: HttpClient) {
  }


  
  getAllMedecinSpec(): Observable<any> {
    return this.http.get(this.baseUrl+'/getAllMedecinSpecialiste');
  }

  
  saveMedecinSpec(medecin:MedSpecialiste):Observable<any>{
    return this.http.post(this.baseUrl+'/addMedecinSpecialiste',medecin);
  }
                
  updateMedecinSpecialiste(id:number,medecin:MedSpecialiste):Observable<any>{
    return this.http.put(this.baseUrl+'/updateMedecinSpecialiste/'+id,medecin);
}

  deleteMedecinSpec(id:number){ 
    return this.http.delete(this.baseUrl+'/deleteMedecinSpecialiste/'+id)
    .map((response:Response)=>response.json()) 
    .catch(this.errorHandle);   
  } 
        
    //afficher en cas d'erreur
  errorHandle(error:Response){
    return Observable.throw(error||"Server Error");
  }
    //getters & setters pour l'edit
  setter(medecin:MedSpecialiste){
    this.medecin=medecin;
  }
  getter(){
    return this.medecin;
  }   
}
