import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutMedGenComponent } from './ajout-med-gen.component';

describe('AjoutMedGenComponent', () => {
  let component: AjoutMedGenComponent;
  let fixture: ComponentFixture<AjoutMedGenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutMedGenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutMedGenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
