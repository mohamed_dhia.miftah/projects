import { Component, OnInit } from '@angular/core';
import { MedGeneraliste } from 'app/modal/med-generaliste';
import { GeneralisteService } from 'app/service/generaliste-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ajout-med-gen',
  templateUrl: './ajout-med-gen.component.html',
  styleUrls: ['./ajout-med-gen.component.scss']
})
export class AjoutMedGenComponent implements OnInit {
  
  private medecin:MedGeneraliste;

  constructor(private medecinService:GeneralisteService, private router:Router) { }
 ngOnInit() {
 this.medecin=this.medecinService.getter();
 }

 saveForm(){
  this.medecinService.saveMedecinGen(this.medecin).subscribe((medecin)=>{
    this.router.navigate(['/tableMed/MedGeneraliste']);
   },(error)=>{console.log(error);});
  }   

}
