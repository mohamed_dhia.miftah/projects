import { Component, OnInit } from '@angular/core';
import { MedGeneraliste } from 'app/modal/med-generaliste';
import { GeneralisteService } from 'app/service/generaliste-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-med-gen',
  templateUrl: './edit-med-gen.component.html',
  styleUrls: ['./edit-med-gen.component.scss']
})
export class EditMedGenComponent implements OnInit {

  private medecin:MedGeneraliste;

  constructor(private genService:GeneralisteService, private router:Router) { }
 ngOnInit() {
 this.medecin=this.genService.getter();
 }

 saveForm(){
   this.genService.updateMedecinGen(this.medecin.id,this.medecin).subscribe((medecin)=>{
    this.medecin = medecin;
   this.router.navigate(['/tableMed/MedGeneraliste']);
   },(error)=>{console.log(error);});
  }
}
