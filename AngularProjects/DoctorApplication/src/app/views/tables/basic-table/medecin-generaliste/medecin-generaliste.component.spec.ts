import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedecinGeneralisteComponent } from './medecin-generaliste.component';

describe('MedecinGeneralisteComponent', () => {
  let component: MedecinGeneralisteComponent;
  let fixture: ComponentFixture<MedecinGeneralisteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedecinGeneralisteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedecinGeneralisteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
