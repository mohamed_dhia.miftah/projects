import { Component, OnInit } from '@angular/core';
import { MedGeneraliste } from 'app/modal/med-generaliste';
import { GeneralisteService } from 'app/service/generaliste-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-medecin-generaliste',
  templateUrl: './medecin-generaliste.component.html',
  styleUrls: ['./medecin-generaliste.component.scss']
})
export class MedecinGeneralisteComponent implements OnInit {

  medecins: Array<any>;
  medecinsGen:MedGeneraliste[];
  medecin:MedGeneraliste;


  constructor(private medecinService: GeneralisteService, private router:Router) { }

  ngOnInit() {
    this.medecinService.getAllMedecinGen().subscribe(data => {
      this.medecins = data;
    },(error)=>{console.log(error);
  });
}

deleteMedecin(medecin){
  this.medecinService.deleteMedecinGen(medecin.id).subscribe((data)=>{
    this.medecinsGen.splice(this.medecinsGen.indexOf(medecin),1);
  });
  location.reload();
}

editMedecinGen(medecin) {
  // setter la personne
  this.medecinService.setter(medecin);
  this.router.navigate(['/MedGen/modifierMedGen']);
  }

  addMedGen() {
    let medecin=new MedGeneraliste();
    this.medecinService.setter(medecin);
    this.router.navigate(['MedGen/ajouterMedGen'])
 }
  getAllSpec()
  {
      this.router.navigate(['/tableMed/MedSpecialiste']);

  }
}
