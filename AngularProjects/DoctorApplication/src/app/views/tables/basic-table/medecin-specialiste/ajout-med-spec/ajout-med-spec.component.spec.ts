import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutMedSpecComponent } from './ajout-med-spec.component';

describe('AjoutMedSpecComponent', () => {
  let component: AjoutMedSpecComponent;
  let fixture: ComponentFixture<AjoutMedSpecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutMedSpecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutMedSpecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
