import { Component, OnInit } from '@angular/core';
import { MedSpecialiste } from 'app/modal/med-specialiste';
import { SpecialisteService } from 'app/service/specialiste-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-med-spec',
  templateUrl: './edit-med-spec.component.html',
  styleUrls: ['./edit-med-spec.component.scss']
})
export class EditMedSpecComponent implements OnInit {

  private medecin:MedSpecialiste;

  constructor(private specService:SpecialisteService, private router:Router) { }
 ngOnInit() {
 this.medecin=this.specService.getter();
 }

 saveForm(){
   this.specService.updateMedecinSpecialiste(this.medecin.id,this.medecin).subscribe((medecin)=>{
    this.medecin = medecin;
   this.router.navigate(['/tableMed/MedSpecialiste']);
   },(error)=>{console.log(error);});
  }
}
