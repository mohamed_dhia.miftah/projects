import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedecinSpecialisteComponent } from './medecin-specialiste.component';

describe('MedecinSpecialisteComponent', () => {
  let component: MedecinSpecialisteComponent;
  let fixture: ComponentFixture<MedecinSpecialisteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedecinSpecialisteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedecinSpecialisteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
