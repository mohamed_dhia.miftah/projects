import { Component, OnInit } from '@angular/core';
import { MedSpecialiste } from 'app/modal/med-specialiste';
import { SpecialisteService } from 'app/service/specialiste-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-medecin-specialiste',
  templateUrl: './medecin-specialiste.component.html',
  styleUrls: ['./medecin-specialiste.component.scss']
})
export class MedecinSpecialisteComponent implements OnInit {
  medecins: Array<any>;
  medecinsF:MedSpecialiste[];
  medecin:MedSpecialiste;


  constructor(private medecinService: SpecialisteService, private router:Router) { }

  ngOnInit() {
    this.medecinService.getAllMedecinSpec().subscribe(data => {
      this.medecins = data;
    },(error)=>{console.log(error);
  });
}

deleteMedecin(medecin){
  this.medecinService.deleteMedecinSpec(medecin.id).subscribe((data)=>{
    this.medecinsF.splice(this.medecinsF.indexOf(medecin),1);
  });
  location.reload();
}

editMedecinSpec(medecin) {
  // setter la personne
  this.medecinService.setter(medecin);
  this.router.navigate(['/MedSpec/modifierMedSpec']);
  }

  addMedSpec() {
    let patient=new MedSpecialiste();
    this.medecinService.setter(patient);
    this.router.navigate(['MedSpec/ajouterMedSpec'])
 }
  getAllGen()
  {
      this.router.navigate(['/tableMed/MedGeneraliste']);
  }
}
