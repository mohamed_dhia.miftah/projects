import { Component, OnInit } from '@angular/core';
import { Consultation } from 'app/modal/consultation';
import { ConsultationService } from 'app/service/consultation-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ajout-consultation',
  templateUrl: './ajout-consultation.component.html',
  styleUrls: ['./ajout-consultation.component.scss']
})
export class AjoutConsultationComponent implements OnInit {

  private consultation:Consultation;

  constructor(private consultationService:ConsultationService, private router:Router) { }
 ngOnInit() {
 this.consultation=this.consultationService.getter();
 }

 saveForm2(){
  this.consultationService.saveConsultation(this.consultation).subscribe((consultation)=>{
    this.router.navigate(['/tables/table-consultation']);
   },(error)=>{console.log(error);});
  }   
}
