import { Component, OnInit } from '@angular/core';
import { Consultation } from 'app/modal/consultation';
import { ConsultationService } from 'app/service/consultation-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-consultation',
  templateUrl: './edit-consultation.component.html',
  styleUrls: ['./edit-consultation.component.scss']
})
export class EditConsultationComponent implements OnInit {

  private consultation:Consultation;

  constructor(private consultationService:ConsultationService, private router:Router) { }
 ngOnInit() {
 this.consultation=this.consultationService.getter();
 }

 saveForm(){
   this.consultationService.updateConsultation(this.consultation.id,this.consultation).subscribe((consultation)=>{
    this.consultation = consultation;
   this.router.navigate(['/tables/table-consultation']);
   },(error)=>{console.log(error);});
   // fin else
   }

}
