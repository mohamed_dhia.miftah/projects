import { Component, OnInit } from '@angular/core';
import { Consultation } from 'app/modal/consultation';
import { ConsultationService } from 'app/service/consultation-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-table-consultation',
  templateUrl: './table-consultation.component.html',
  styleUrls: ['./table-consultation.component.scss']
})
export class TableConsultationComponent implements OnInit {

  consultations: Array<any>;
  private consultation:Consultation;
  consultationAdd:Consultation;
  consultationF:Consultation[];

  constructor(private consultationService: ConsultationService, private router:Router) { 

  }
  
  
  ngOnInit() {
    this.consultationService.getConsultations().subscribe((data) => {
      this.consultations = data;
    },(error)=>{console.log(error);
  });
  //this.patient=this.patientService.getter();
 
}

  deleteConsultation(consultation){
    this.consultationService.deleteConsultation(consultation.id).subscribe((data)=>{
      this.consultationF.splice(this.consultationF.indexOf(consultation),1);
    });
    location.reload();
  }
  
  editConsultation(consultation) {
    // setter la personne
    this.consultationService.setter(consultation);
    this.router.navigate(['/tableCons/modifierCons']);
    }

     
  addConsultation() {
        let consultation=new Consultation();
        this.consultationService.setter(consultation);
        this.router.navigate(['tableCons/ajoutCons'])
     }
      

}
