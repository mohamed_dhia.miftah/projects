import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFormPatientComponent } from './edit-form-patient.component';

describe('EditFormPatientComponent', () => {
  let component: EditFormPatientComponent;
  let fixture: ComponentFixture<EditFormPatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditFormPatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFormPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
