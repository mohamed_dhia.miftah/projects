import { Component, OnInit } from '@angular/core';
import { Patient } from 'app/modal/patient';
import { PatientService } from 'app/service/patient-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-form-patient',
  templateUrl: './edit-form-patient.component.html',
  styleUrls: ['./edit-form-patient.component.scss']
})
export class EditFormPatientComponent implements OnInit {

  private patient:Patient;

  constructor(private patientService:PatientService, private router:Router) { }
 ngOnInit() {
 this.patient=this.patientService.getter();
 }

 saveForm(){
   this.patientService.updatePatient(this.patient.id,this.patient).subscribe((patient)=>{
    this.patient = patient;
   this.router.navigate(['/tables/table-patient']);
   },(error)=>{console.log(error);});
   }
}
