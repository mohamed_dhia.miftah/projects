import { Component, OnInit } from '@angular/core';
import { PatientService } from 'app/service/patient-service';
import { Router } from '@angular/router';
import { Patient } from 'app/modal/patient';

@Component({
  selector: 'app-table-patient',
  templateUrl: './table-patient.component.html',
  styleUrls: ['./table-patient.component.scss']
})
export class TablePatientComponent implements OnInit {

  patients: Array<any>;
  patientsF:Patient[];

  constructor(private patientService: PatientService, private router:Router) {
  }

  ngOnInit() {
    this.patientService.getAllPatients().subscribe((data) => {
      this.patients = data;
    },(error)=>{console.log(error);
  });
}

  deletePatient(patient){
    this.patientService.deletePatient(patient.id).subscribe((data)=>{
      this.patientsF.splice(this.patientsF.indexOf(patient),1);
    });
    location.reload();
  }
  
  editPatient(patient) {
    // setter la personne
    this.patientService.setter(patient);
    this.router.navigate(['/tablePat/modifierPat']);
  }

  addPatient() {
    let patient=new Patient();
    this.patientService.setter(patient);
    this.router.navigate(['tablePat/ajoutPat'])
  }
}


