import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AgmCoreModule } from '@agm/core';

import { CalendarModule } from 'angular-calendar';

import { SharedModule } from '../shared/shared.module';

import { FooterComponent } from '../main-layout/footer/footer.component';
import { BasicTableComponent } from './tables/basic-table/basic-table.component';
import { ModalsComponent } from './modals/modals.component';
import { TypographyComponent } from './css/typography/typography.component';
import { IconsComponent } from './css/icons/icons.component';
import { Map1Component } from './maps/map1/map1.component';
import { StatsCardComponent } from './dashboards/common/stats-card/stats-card.component';
import { StatsCard2Component } from './dashboards/common/stats-card2/stats-card2.component';
import { Dashboard1Component } from './dashboards/dashboard1/dashboard1.component';
import { GridComponent } from './css/grid/grid.component';
import { MediaObjectComponent } from './css/media-object/media-object.component';
import { UtilitiesComponent } from './css/utilities/utilities.component';
import { ImagesComponent } from './css/images/images.component';
import { ColorsComponent } from './css/colors/colors.component';
import { ShadowComponent } from './css/shadow/shadow.component';
import { Profile1Component } from './profile/profile1/profile1.component';
import { HelpComponent } from './help/help.component';
import { TablePatientComponent } from './tables/table-patient/table-patient.component';
import { TableConsultationComponent } from './tables/table-consultation/table-consultation.component';
import { FormEditMedecinComponent } from './tables/basic-table/form-edit-medecin/form-edit-medecin.component';
import { EditFormPatientComponent } from './tables/table-patient/edit-form-patient/edit-form-patient.component';
import { NewPatientComponent } from './tables/table-patient/new-patient/new-patient.component';
import { MedecinSpecialisteComponent } from './tables/basic-table/medecin-specialiste/medecin-specialiste.component';
import { MedecinGeneralisteComponent } from './tables/basic-table/medecin-generaliste/medecin-generaliste.component';
import { EditMedSpecComponent } from './tables/basic-table/medecin-specialiste/edit-med-spec/edit-med-spec.component';
import { AjoutMedSpecComponent } from './tables/basic-table/medecin-specialiste/ajout-med-spec/ajout-med-spec.component';
import { AjoutMedGenComponent } from './tables/basic-table/medecin-generaliste/ajout-med-gen/ajout-med-gen.component';
import { EditMedGenComponent } from './tables/basic-table/medecin-generaliste/edit-med-gen/edit-med-gen.component';
import { AjoutConsultationComponent } from './tables/table-consultation/ajout-consultation/ajout-consultation.component';
import { EditConsultationComponent } from './tables/table-consultation/edit-consultation/edit-consultation.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    AgmCoreModule.forRoot({
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
      apiKey: ''
    }),
    CalendarModule.forRoot()
  ],
  declarations: [
    FooterComponent,
    BasicTableComponent,
    ModalsComponent,
    TypographyComponent,
    IconsComponent,
    Map1Component,
    StatsCardComponent,
    StatsCard2Component,
    Dashboard1Component,
    GridComponent,
    MediaObjectComponent,
    UtilitiesComponent,
    ImagesComponent,
    ColorsComponent,
    ShadowComponent,
    Profile1Component,
    HelpComponent,
    TablePatientComponent,
    TableConsultationComponent,
    FormEditMedecinComponent,
    EditFormPatientComponent,
    NewPatientComponent,
    MedecinSpecialisteComponent,
    MedecinGeneralisteComponent,
    EditMedSpecComponent,
    AjoutMedSpecComponent,
    AjoutMedGenComponent,
    EditMedGenComponent,
    AjoutConsultationComponent,
    EditConsultationComponent,
    

  ],
  exports: [
    FooterComponent,
    BasicTableComponent,
    ModalsComponent,
    TypographyComponent,
    IconsComponent,
    Map1Component,
    StatsCardComponent,
    StatsCard2Component,    
    Dashboard1Component,
    GridComponent,
    MediaObjectComponent,
    UtilitiesComponent,
    ImagesComponent,
    ColorsComponent,
    ShadowComponent,

  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ViewsModule { }
