// free
import { ModuleWithProviders, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { CardsFreeModule } from './cards/cards.module';
import { ButtonsModule } from './buttons/buttons.module';
import { RippleModule } from './ripple/ripple.module';
import { InputsModule } from './inputs/inputs.module';
import { NavbarModule } from './navbars/navbar.module';
import { DropdownModule } from './dropdown/dropdown.module';
import { CarouselModule } from './carousel/carousel.module';
import { ChartsModule } from './charts/chart.module';
import { CollapseModule } from './collapse/collapse.module';
import { ModalModule } from './modals/modal.module';
import { TooltipModule } from './tooltip/tooltip.module';
import { PopoverModule } from './popover/popover.module';
import { WavesModule } from './waves/waves.module';
import { IconsModule } from './icons/icon.module';
import { CheckboxModule } from './checkbox/checkbox.module';
import { TableModule } from './tables/tables.module';
import { BadgeModule } from './badge/badge.module';
import { BreadcrumbModule } from './breadcrumbs/breadcrumb.module';

export {
  MdbBreadcrumbComponent, MdbBreadcrumbItemComponent, BreadcrumbModule
} from './breadcrumbs/';

export {
  MDBBadgeComponent, BadgeModule
} from './badge/';

export {
  MdbTablePaginationComponent, MdbTableRowDirective, MdbTableScrollDirective,
  MdbTableSortDirective, MdbTableDirective, MdbTableService
} from './tables/';

export {
  ButtonsModule, ButtonRadioDirective, ButtonCheckboxDirective, MdbBtnDirective
} from './buttons/';

export {
  CHECKBOX_VALUE_ACCESSOR, CheckboxComponent, CheckboxModule
} from './checkbox/';

export {
  CardsFreeModule,
  MdbCardComponent,
  MdbCardBodyComponent,
  MdbCardImageComponent,
  MdbCardTextComponent,
  MdbCardTitleComponent,
  MdbCardFooterComponent,
  MdbCardHeaderComponent
} from './cards/';

export {
  RippleModule, RippleDirective
} from './ripple/';

export {
  WavesModule, WavesDirective
} from './waves/';

export {
  InputsModule, MdbInputDirective
} from './inputs/';

export {
  NavbarModule
} from './navbars/';

export {
  BsDropdownConfig, BsDropdownContainerComponent, BsDropdownDirective, BsDropdownMenuDirective,
  DropdownModule, BsDropdownState, BsDropdownToggleDirective
} from './dropdown/';

export {
  CarouselComponent, CarouselConfig, CarouselModule
} from './carousel/';

export {
  ChartsModule, BaseChartDirective
} from './charts/';

export {
  CollapseComponent, CollapseModule
} from './collapse/';

export {
  ModalBackdropComponent, ModalBackdropOptions, ModalDirective, ModalModule, ModalOptions, MDBModalService,
  ModalContainerComponent, MDBModalRef
} from './modals/';

export {
  TooltipConfig, TooltipContainerComponent, TooltipDirective, TooltipModule
} from './tooltip/';

export {
  PopoverConfig, PopoverContainerComponent, PopoverModule, PopoverDirective
} from './popover/';

export {
  IconsModule, MdbIconComponent
} from './icons/';



const MODULES = [
  ButtonsModule,
  CardsFreeModule,
  RippleModule,
  WavesModule,
  InputsModule,
  NavbarModule,
  DropdownModule,
  CarouselModule,
  ChartsModule,
  CollapseModule,
  ModalModule,
  TooltipModule,
  PopoverModule,
  IconsModule,
  CheckboxModule,
  TableModule,
  BadgeModule,
  BreadcrumbModule

];

@NgModule({
  imports: [
    ButtonsModule,
    RippleModule.forRoot(),
    WavesModule.forRoot(),
    InputsModule.forRoot(),
    NavbarModule,
    DropdownModule.forRoot(),
    CarouselModule.forRoot(),
    ChartsModule,
    CollapseModule.forRoot(),
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    PopoverModule.forRoot(),
    IconsModule,
    CardsFreeModule.forRoot(),
    CheckboxModule,
    TableModule,
    BadgeModule,
    BreadcrumbModule
  ],
  exports: MODULES,
  schemas: [NO_ERRORS_SCHEMA]
})
export class MDBRootModule {
}

@NgModule({ exports: MODULES })
export class MDBBootstrapModule {
  public static forRoot(): ModuleWithProviders {
    return { ngModule: MDBRootModule };
  }
}
