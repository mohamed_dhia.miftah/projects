import {Component} from '@angular/core';

// Import the functions you need from the SDKs you need
import {initializeApp} from 'firebase/app';
import {getAnalytics} from 'firebase/analytics';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'bookShelves';

    constructor() {
        const firebaseConfig = {
            apiKey: "AIzaSyD0VS8T6BOlqYIZoj4Uig2fVw7oy_CyacY",
            authDomain: "deviceapplicationfirebase.firebaseapp.com",
            databaseURL: "https://deviceapplicationfirebase-default-rtdb.europe-west1.firebasedatabase.app",
            projectId: "deviceapplicationfirebase",
            storageBucket: "deviceapplicationfirebase.appspot.com",
            messagingSenderId: "68484714729",
            appId: "1:68484714729:web:2b8a9dbb244de94ad86078",
            measurementId: "G-4FN6J7FHNQ"
        };

        // Initialize Firebase
        const app = initializeApp(firebaseConfig);
        const analytics = getAnalytics(app);

        // Scripts
        // firebase login
        // firebase init
        // firebase deploy
    }
}
