import {Injectable} from '@angular/core';
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {getAuth, onAuthStateChanged} from "firebase/auth";

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService {

    constructor(private router: Router) {
    }

    canActivate(): Observable<boolean> |Promise<boolean> | boolean {
        return new Promise(
            ((resolve, reject) => {

                const auth = getAuth();
                onAuthStateChanged(auth, (user) => {
                    console.log('user = ',user)
                    if (user) {
                        resolve(true);
                    } else {
                        this.router.navigate(['/auth','signin']);
                        resolve(false);
                    }
                });
            })
        )
    }
}
