import {Injectable} from '@angular/core';
import {getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut} from "firebase/auth";
import firebase from "firebase/compat";

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    isAuth: boolean = false;

    constructor() {
    }

    createNewUser(email: string, password: string) {
        return new Promise<void>(
            (resolve, reject) => {
                /*firebase.auth().createUserWithEmailAndPassword(email, password).then(
                    () => {
                        resolve();
                    },
                    (error) => {
                        reject(error);
                    }
                )*/


                const auth = getAuth();
                createUserWithEmailAndPassword(auth, email, password)
                    .then(
                        (userCredential) => {
                            // Signed in
                            const user = userCredential.user;
                            resolve();
                        },
                        (error) => {
                            reject(error);
                        }
                    )
                    .catch((error) => {
                        const errorCode = error.code;
                        const errorMessage = error.message;
                        console.log('Error response ! ' + error);

                    });
            }
        );
        // const auth = getAuth();
        // createUserWithEmailAndPassword(auth, email, password)
        //     .then(
        //         (userCredential) => {
        //             // Signed in
        //             const user = userCredential.user;
        //         },
        //     )
        //     .catch((error) => {
        //         const errorCode = error.code;
        //         const errorMessage = error.message;
        //         console.log('Error response ! ' + error);
        //     });
    }

    signInUser(email: string, password: string) {
        return new Promise<void>((resolve, reject) => {
                const auth = getAuth();
                signInWithEmailAndPassword(auth, email, password)
                    .then(
                        (userCredential) => {
                            // Signed in
                            const user = userCredential.user;
                            if (user) {
                                this.isAuth = true;
                            } else {
                                this.isAuth = false;
                            }
                            console.log('signIn success ! ');
                            resolve();
                        },
                        (error) => {
                            reject(error);
                        })
                    .catch((error) => {
                        const errorCode = error.code;
                        const errorMessage = error.message;
                        console.log('Error response ! ' + error);
                    });
            }
        )
    }

    signOutUser() {
        const auth = getAuth();
        signOut(auth).then(() => {
            console.log('signOut success ! ');
            this.isAuth = false;

        }).catch((error) => {
            console.log('Error response ! ' + error);
        });
    }
}
