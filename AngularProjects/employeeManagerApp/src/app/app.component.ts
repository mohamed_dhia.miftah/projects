import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Employee } from './model/employee';
import { EmployeeService } from './service/employee.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	title = 'employeeManagerApp';

	public employeesList: Employee[] = [];

	constructor(private employeeService: EmployeeService) { }
	ngOnInit(): void {
		this.getEmployees();
	}

	/**
	 * getEmployees
	 */
	public getEmployees(): void {
		this.employeeService.getEmployees().subscribe(
			(response: Employee[]) => { this.employeesList = response },
			(error: HttpErrorResponse) => { alert(error.message) }
		);
	}
}
