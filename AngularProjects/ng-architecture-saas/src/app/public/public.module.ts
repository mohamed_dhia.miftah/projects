import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { HomeModule } from '../home/home.module';
import { LoginModule } from '../public/login/login.module';
import { SignInModule } from '../public/sign-in/sign-in.module';
import { SharedPublicModule } from '../public/shared-public/shared-public.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PublicRoutingModule,
    HomeModule,
    LoginModule,
    SignInModule,
    SharedPublicModule
  ]
})
export class PublicModule { }
