import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedPublicRoutingModule } from './shared-public-routing.module';
import { SomePipePipe } from './pipe/some-pipe.pipe';


@NgModule({
  declarations: [
    SomePipePipe
  ],
  imports: [
    CommonModule,
    SharedPublicRoutingModule
  ],
  exports: [
    SomePipePipe
  ]
})
export class SharedPublicModule { }
