package tn.com.Compte.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tn.com.Compte.entities.Compte;
import tn.com.Compte.repositories.ICompteRepository;

@Service
@Transactional
public class CompteService implements ICompteService {

	@Autowired
	private ICompteRepository compteRepository;

	@Override
	public void virement(Long codeSource, Long codeDestiantion, double montant) {
		Compte c1 = compteRepository.getById(codeSource);
		Compte c2 = compteRepository.getById(codeDestiantion);
		
		c1.setSolde(c1.getSolde() - montant);
		c2.setSolde(c2.getSolde() + montant);
		
		compteRepository.save(c1);
		compteRepository.save(c2);
	}

}
