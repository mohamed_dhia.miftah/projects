package tn.com.Compte.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tn.com.Compte.DTOs.VirementRequestDTO;
import tn.com.Compte.service.ICompteService;

@RestController
public class AcountRestController {

	 @Autowired
	 private ICompteService compteService;
	 
	 @PutMapping(path = "comptes/virement")
	 public void virement(@RequestBody VirementRequestDTO requestDTO) {
		 compteService.virement(requestDTO.getCodeSource(), requestDTO.getCodeDestination(), requestDTO.getAmount());
	 }
	
}
