package tn.com.Compte.web;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tn.com.Compte.entities.Compte;
import tn.com.Compte.repositories.ICompteRepository;

// @RestController
public class CompteRestController {

	// @Autowired
	private ICompteRepository compteRepository;

	public CompteRestController(ICompteRepository compteRepository) {
		this.compteRepository = compteRepository;
	}

	@GetMapping(path = "/comptes")
	public List<Compte> listComptes() {
		return compteRepository.findAll();
	}

	@GetMapping(path = "/comptes/{id}")
	public Compte getCompte(@PathVariable(name = "id") Long compteId) {
		return compteRepository.findById(compteId).get();
	}

	@PostMapping(path = "/comptes")
	public Compte saveCompte(Compte compte) {
		return compteRepository.save(compte);
	}

	@PutMapping(path = "/comptes/{code}")
	public Compte updateCompte(@PathVariable Long code, @RequestBody Compte compte) {
		compte.setCode(code);
		return compteRepository.save(compte);
	}

	@DeleteMapping(path = "/comptes/{code}")
	public void deleteCompte(@PathVariable Long code) {
		compteRepository.deleteById(code);
	}
}
