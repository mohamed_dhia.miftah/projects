package tn.enis.SpringProject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import tn.enis.SpringProject.dao.ConsultationRepository;
import tn.enis.SpringProject.dao.MedecinGeneralisteRepository;
import tn.enis.SpringProject.dao.MedecinRepository;
import tn.enis.SpringProject.dao.MedecinSpecialisteRepository;
import tn.enis.SpringProject.dao.PatientRepository;
import tn.enis.SpringProject.dao.UserRepository;

@SpringBootApplication
public class AngularSpringProjectApplication implements CommandLineRunner {

	@Autowired
	MedecinRepository medecinRepository;
	@Autowired
	MedecinGeneralisteRepository medecinGeneralisteRepository;
	@Autowired
	MedecinSpecialisteRepository medecinSpecialisteRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	PatientRepository patientRepository;
	@Autowired
	ConsultationRepository consultationRepository;

	public static void main(String[] args) {
		SpringApplication.run(AngularSpringProjectApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
//		Medecin medecin = new MedecinSpecialiste("nom medecin 2", "prenom medecin 2", new Date(), "medecin2@gmail.com", null, "2222 2222", "optecien");
//		medecinRepository.save(medecin);
		
//		Patient patient1 = new Patient("hdfh", "gdg", new Date(), null, "hfdif@sjhq.com", "12345678");
//		patientRepository.save(patient1);
//		patientRepository.delete(patient1);

	}
}
