package tn.enis.SpringProject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.enis.SpringProject.entities.Medecin;
import tn.enis.SpringProject.service.MedecinService;

@RestController
@RequestMapping("/cabinet")
@CrossOrigin("*")
public class MedecinController {

	@Autowired
	MedecinService medecinService;

	@GetMapping("/getAllMedecin")
	public List<Medecin> getAllMedecin() {
		return medecinService.getAllMedecin();
	}

	@GetMapping(value = "/getOneMedecin/{id}")
	public Medecin getOneMedecin(@PathVariable Long id) {
		return medecinService.getOneMedecin(id);
	}

	@GetMapping("/findMedecinByLastName/{nom}")
	public List<Medecin> findMedecinByLastName(@PathVariable String nom) {
		return medecinService.findMedecinByLastName(nom);
	}

	@GetMapping("/findMedecinByName/{prenom}")
	public List<Medecin> findMedecinByName(@PathVariable String prenom) {
		return medecinService.findMedecinByName(prenom);
	}

	@GetMapping("/sortMedecinByLastName")
	public List<Medecin> sortMedecinByLastName() {
		return medecinService.sortByLastName();
	}

	@GetMapping("/sortMedecinByName")
	public List<Medecin> sortMedecinByName() {
		return medecinService.sortByName();
	}

	@DeleteMapping("/deleteMedecin/{id}")
	public void deleteMedecin(@PathVariable Long id) {
		medecinService.deleteMedecin(id);
	}
}