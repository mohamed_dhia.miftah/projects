package tn.enis.SpringProject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.enis.SpringProject.entities.MedecinSpecialiste;
import tn.enis.SpringProject.service.MedecinSpecialisteService;

@RestController
@RequestMapping("/cabinet")
@CrossOrigin("*")
public class MedecinSpecialisteController {

	@Autowired
	MedecinSpecialisteService medecinSpecialisteService;

	@GetMapping(value = "/getOneMedecinSpecialiste/{id}")
	public MedecinSpecialiste getOneMedecinSpecialiste(@PathVariable Long id) {
		return medecinSpecialisteService.getOneMedecinSpecialiste(id);
	}

	@GetMapping("/getAllMedecinSpecialiste")
	public List<MedecinSpecialiste> getAllMedecinSpecialiste() {
		return medecinSpecialisteService.getAllMedecinSpecialiste();
	}

	@PostMapping("/addMedecinSpecialiste")
	public void addMedecinSpecialiste(@RequestBody MedecinSpecialiste medecinSpecialiste) {
		medecinSpecialisteService.addMedecinSpecialiste(medecinSpecialiste);
	}

	@PutMapping(value = "/updateMedecinSpecialiste/{id}")
	public void UpdateMedecinSpecialiste(@PathVariable Long id, @RequestBody MedecinSpecialiste medecinSpecialiste) {
		medecinSpecialisteService.updateMedecinSpecialiste(id, medecinSpecialiste);
	}

	@DeleteMapping("/deleteMedecinSpecialiste/{id}")
	public void deleteMedecinSpecialiste(@PathVariable Long id) {
		medecinSpecialisteService.deleteMedecinSpecialiste(id);
	}
}
