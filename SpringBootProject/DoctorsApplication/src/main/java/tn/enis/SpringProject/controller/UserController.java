package tn.enis.SpringProject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.enis.SpringProject.entities.User;
import tn.enis.SpringProject.service.UserService;

@RestController
@RequestMapping("/cabinet")
@CrossOrigin("*")
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping(value = "/getOneUser/{username}")
	public User getOneUser(@PathVariable String username) {
		return userService.getOneUser(username);
	}

	@GetMapping("/getAllUser")
	public List<User> getAllUser() {
		return userService.getAllUser();
	}

	@PostMapping("/addUser")
	public void addUser(@RequestBody User user) {
		userService.addUser(user);
	}

	@PutMapping(value = "/updateUser/{username}")
	public void UpdateUser(@PathVariable String username, @RequestBody User user) {
		user.setUsername(username);
		userService.updateUser(username, user);
	}

	@DeleteMapping("/deleteUser/{username}")
	public void deleteUser(@PathVariable String username) {
		userService.deleteUser(username);
	}
}
