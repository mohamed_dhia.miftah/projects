package tn.enis.SpringProject.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import tn.enis.SpringProject.entities.User;

public interface UserRepository extends JpaRepository<User, String> {

}
