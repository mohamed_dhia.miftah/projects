package tn.enis.SpringProject.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.enis.SpringProject.dao.ConsultationRepository;
import tn.enis.SpringProject.entities.Consultation;

@Service
public class ConsultationService implements IConsultationService {

	@Autowired
	ConsultationRepository consultantRepository;

	@Override
	public Consultation getOneConsultation(Long id) {
		return consultantRepository.findById(id).get();
	}

	@Override
	public List<Consultation> getAllConsultation() {
		return consultantRepository.findAll();
	}

	@Override
	public void addConsultation(Consultation consultation) {
		consultantRepository.saveAndFlush(consultation);
	}

	@Override
	public void updateConsultation(Long id, Consultation consultation) {
		consultation.setId(id);
		consultantRepository.saveAndFlush(consultation);
	}

	@Override
	public String deleteConsultation(Long id) {
		consultantRepository.deleteById(id);
		consultantRepository.flush();
		return "Consultation deleted";
	}
	
	@Override
	public List<Consultation> sortByDate() {
		return consultantRepository.sortByDate();
	}

}
