package tn.enis.SpringProject.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.enis.SpringProject.dao.PatientRepository;
import tn.enis.SpringProject.entities.Patient;

@Service
public class PatientService implements IPatientService {

	@Autowired
	PatientRepository patientRepository;

	@Override
	public Patient getOnePatient(Long id) {
		return patientRepository.findById(id).get();
	}

	@Override
	public List<Patient> getAllPatient() {
		return patientRepository.findAll();
	}

	@Override
	public void addPatient(Patient patient) {
		patientRepository.saveAndFlush(patient);
	}

	@Override
	public void updatePatient(Long id, Patient patient) {
		patient.setId(id);
		patientRepository.saveAndFlush(patient);
	}

	@Override
	public String deletePatient(Long id) {
		patientRepository.deleteById(id);
		patientRepository.flush();
		return "patient supprimé avec succée";
	}

	@Override
	public List<Patient> findPatientByLastName(String nom) {
		return patientRepository.findPatientByLastName(nom);
	}

	@Override
	public List<Patient> findPatientByName(String prenom) {
		return patientRepository.findPatientByName(prenom);
	}
	
	@Override
	public List<Patient> sortByLastName() {
		return patientRepository.sortByLastName();
	}

	@Override
	public List<Patient> sortByName() {
		return patientRepository.sortByName();
	}
}
