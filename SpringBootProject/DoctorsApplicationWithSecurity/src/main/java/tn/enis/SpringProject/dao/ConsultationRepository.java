package tn.enis.SpringProject.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import tn.enis.SpringProject.entities.Consultation;

public interface ConsultationRepository extends JpaRepository<Consultation, Long> {
	
	@Query("select c from Consultation c order by date DESC")
	public List<Consultation> sortByDate();

}
