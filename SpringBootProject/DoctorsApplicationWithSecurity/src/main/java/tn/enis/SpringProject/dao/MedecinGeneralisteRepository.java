package tn.enis.SpringProject.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import tn.enis.SpringProject.entities.MedecinGeneraliste;

public interface MedecinGeneralisteRepository extends JpaRepository<MedecinGeneraliste, Long> {

}
