package tn.enis.SpringProject.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import tn.enis.SpringProject.entities.Patient;

public interface PatientRepository extends JpaRepository<Patient, Long> {

	@Query("SELECT p FROM Patient p WHERE LOWER(p.nom) like concat(LOWER(:nom),'%')")
	public List<Patient> findPatientByLastName(@Param("nom") String nom);

	@Query("SELECT p FROM Patient p WHERE LOWER(p.prenom) like concat(LOWER(:prenom),'%')")
	public List<Patient> findPatientByName(@Param("prenom") String prenom);

	@Query("select p from Patient p order by nom ASC")
	public List<Patient> sortByLastName();

	@Query("select p from Patient p order by prenom ASC")
	public List<Patient> sortByName();
}
