package tn.enis.SpringProject.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@DiscriminatorValue("MG")
public class MedecinGeneraliste extends Medecin implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@OneToMany(mappedBy = "generaliste", cascade = CascadeType.ALL)
	private Collection<Patient> patients;

	public MedecinGeneraliste() {
		super();
	}

	public MedecinGeneraliste(String nom, String prenom, Date dateNaissance, String email, byte[] photo,
			String codePublic) {
		super(nom, prenom, dateNaissance, email, photo, codePublic);
	}

	public Collection<Patient> getPatients() {
		return patients;
	}

	public void setPatients(Collection<Patient> patients) {
		this.patients = patients;
	}
}
