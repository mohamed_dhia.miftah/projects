package tn.enis.SpringProject.service;

import java.util.List;

import tn.enis.SpringProject.entities.MedecinSpecialiste;

public interface IMedecinSpecialisteService {

	public MedecinSpecialiste getOneMedecinSpecialiste(Long id);

	public List<MedecinSpecialiste> getAllMedecinSpecialiste();

	public void addMedecinSpecialiste(MedecinSpecialiste medecinSpecialiste);

	public void updateMedecinSpecialiste(Long id, MedecinSpecialiste medecinSpecialiste);

	public String deleteMedecinSpecialiste(Long id);

}
