package tn.enis.SpringProject.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.enis.SpringProject.dao.MedecinRepository;
import tn.enis.SpringProject.entities.Medecin;

@Service
public class MedecinService implements IMedecinService {

	@Autowired
	MedecinRepository medecinRepository;

	@Override
	public Medecin getOneMedecin(Long id) {
		return medecinRepository.findById(id).get();
	}

	@Override
	public List<Medecin> getAllMedecin() {
		return medecinRepository.findAll();
	}

	@Override
	public String deleteMedecin(Long id) {
		medecinRepository.deleteById(id);
		medecinRepository.flush();
		return "medecin specialiste deleted";
	}

	@Override
	public List<Medecin> findMedecinByLastName(String nom) {
		return medecinRepository.findMedecinByLastName(nom);
	}

	@Override
	public List<Medecin> findMedecinByName(String prenom) {
		return medecinRepository.findMedecinByName(prenom);
	}

	@Override
	public List<Medecin> sortByLastName() {
		return medecinRepository.sortByLastName();
	}

	@Override
	public List<Medecin> sortByName() {
		return medecinRepository.sortByName();
	}
}
