package tn.talan.pfe.XmlDto;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CargoBoxType", propOrder = { "boxId", "deviceAttributes" })
public class CargoBoxTypeDTO {

	@XmlElement(name = "Box_id", required = true)
	protected String boxId;
	@XmlElement(name = "Device_attributes", required = true)
	protected List<DeviceAttributesTypeDTO> deviceAttributes;

	/**
	 * Obtient la valeur de la propri�t� boxId.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBoxId() {
		return boxId;
	}

	/**
	 * D�finit la valeur de la propri�t� boxId.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setBoxId(String value) {
		this.boxId = value;
	}

	/**
	 * Gets the value of the deviceAttributes property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot.
	 * Therefore any modification you make to the returned list will be present
	 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
	 * for the deviceAttributes property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getDeviceAttributes().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link DeviceAttributesTypeDTO }
	 * 
	 * 
	 */
	public List<DeviceAttributesTypeDTO> getDeviceAttributes() {
		if (deviceAttributes == null) {
			deviceAttributes = new ArrayList<DeviceAttributesTypeDTO>();
		}
		return this.deviceAttributes;
	}

}
