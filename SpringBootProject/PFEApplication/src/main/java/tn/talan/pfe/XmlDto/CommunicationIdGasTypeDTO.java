package tn.talan.pfe.XmlDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Communication-id-GasType", propOrder = { "identificationNumber", "manufacturerIdentification",
		"versionIndex", "versionRadio", "deviceTypeIdentification", "devicePINCode" })
public class CommunicationIdGasTypeDTO {

	@XmlElement(name = "Identification_number", required = true)
	protected String identificationNumber;
	@XmlElement(name = "Manufacturer_identification", required = true)
	protected String manufacturerIdentification;
	@XmlElement(name = "Version_Index", required = true)
	protected String versionIndex;
	@XmlElement(name = "Version_Radio", required = true)
	protected String versionRadio;
	@XmlElement(name = "Device_type_identification", required = true)
	protected String deviceTypeIdentification;
	@XmlElement(name = "Device_PIN_code", required = true)
	protected String devicePINCode;

	/**
	 * Obtient la valeur de la propri�t� identificationNumber.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIdentificationNumber() {
		return identificationNumber;
	}

	/**
	 * D�finit la valeur de la propri�t� identificationNumber.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setIdentificationNumber(String value) {
		this.identificationNumber = value;
	}

	/**
	 * Obtient la valeur de la propri�t� manufacturerIdentification.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getManufacturerIdentification() {
		return manufacturerIdentification;
	}

	/**
	 * D�finit la valeur de la propri�t� manufacturerIdentification.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setManufacturerIdentification(String value) {
		this.manufacturerIdentification = value;
	}

	/**
	 * Obtient la valeur de la propri�t� versionIndex.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVersionIndex() {
		return versionIndex;
	}

	/**
	 * D�finit la valeur de la propri�t� versionIndex.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setVersionIndex(String value) {
		this.versionIndex = value;
	}

	/**
	 * Obtient la valeur de la propri�t� versionRadio.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVersionRadio() {
		return versionRadio;
	}

	/**
	 * D�finit la valeur de la propri�t� versionRadio.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setVersionRadio(String value) {
		this.versionRadio = value;
	}

	/**
	 * Obtient la valeur de la propri�t� deviceTypeIdentification.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDeviceTypeIdentification() {
		return deviceTypeIdentification;
	}

	/**
	 * D�finit la valeur de la propri�t� deviceTypeIdentification.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setDeviceTypeIdentification(String value) {
		this.deviceTypeIdentification = value;
	}

	/**
	 * Obtient la valeur de la propri�t� devicePINCode.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDevicePINCode() {
		return devicePINCode;
	}

	/**
	 * D�finit la valeur de la propri�t� devicePINCode.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setDevicePINCode(String value) {
		this.devicePINCode = value;
	}

}
