package tn.talan.pfe.XmlDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DLMSAttributesType", propOrder = { "equipmentIdentifier" })
public class DLMSAttributesTypeDTO {

	@XmlElement(name = "Equipment_identifier", required = true)
	protected String equipmentIdentifier;

	/**
	 * @return possible object is {@link String }
	 */
	public String getEquipmentIdentifier() {
		return equipmentIdentifier;
	}

	/**
	 * @param value allowed object is {@link String }
	 */
	public void setEquipmentIdentifier(String value) {
		this.equipmentIdentifier = value;
	}
}