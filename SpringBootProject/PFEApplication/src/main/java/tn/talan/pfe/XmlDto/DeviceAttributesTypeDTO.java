package tn.talan.pfe.XmlDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Device_attributesType", propOrder = { "general", "mbus", "batteries", "dlmsAttributes" })
public class DeviceAttributesTypeDTO {

	@XmlElement(name = "General", required = true)
	protected GeneralTypeDTO general;
	@XmlElement(name = "Mbus", required = true)
	protected MbusTypeDTO mbus;
	@XmlElement(name = "Batteries", required = true)
	protected BatteriesTypeDTO batteries;
	@XmlElement(name = "DLMSAttributes", required = true)
	protected DLMSAttributesTypeDTO dlmsAttributes;

	/**
	 * @return possible object is {@link GeneralTypeDTO }
	 */
	public GeneralTypeDTO getGeneral() {
		return general;
	}

	/**
	 * @param value allowed object is {@link GeneralTypeDTO }
	 */
	public void setGeneral(GeneralTypeDTO value) {
		this.general = value;
	}

	/**
	 * @return possible object is {@link MbusTypeDTO }
	 */
	public MbusTypeDTO getMbus() {
		return mbus;
	}

	/**
	 * @param value allowed object is {@link MbusTypeDTO }
	 */
	public void setMbus(MbusTypeDTO value) {
		this.mbus = value;
	}

	/**
	 * @return possible object is {@link BatteriesTypeDTO }
	 */
	public BatteriesTypeDTO getBatteries() {
		return batteries;
	}

	/**
	 * @param value allowed object is {@link BatteriesTypeDTO }
	 */
	public void setBatteries(BatteriesTypeDTO value) {
		this.batteries = value;
	}

	/**
	 * @return possible object is {@link DLMSAttributesTypeDTO }
	 */
	public DLMSAttributesTypeDTO getDLMSAttributes() {
		return dlmsAttributes;
	}

	/**
	 * @param value allowed object is {@link DLMSAttributesTypeDTO }
	 */
	public void setDLMSAttributes(DLMSAttributesTypeDTO value) {
		this.dlmsAttributes = value;
	}
}