package tn.talan.pfe.XmlDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeneralType", propOrder = { "serialNumber", "customerArticleNumber", "customerLotNumber", "testReport",
		"certificationYear", "certificationNumberReport", "deviceBatchNumber", "communicationMethod",
		"yearOfManufactory", "hardwareVersion", "coreActiveFirmwareVersion" })
public class GeneralTypeDTO {

	@XmlElement(name = "Serial_number", required = true)
	protected String serialNumber;

	@XmlElement(name = "Customer_Article_Number", required = true)
	protected String customerArticleNumber;

	@XmlElement(name = "Customer_Lot_Number", required = true)
	protected String customerLotNumber;

	@XmlElement(name = "Test_report", required = true)
	protected String testReport;

	@XmlElement(name = "Certification_Year", required = true)
	protected String certificationYear;

	@XmlElement(name = "Certification_Number_Report", required = true)
	protected String certificationNumberReport;

	@XmlElement(name = "Device_batch_number", required = true)
	protected String deviceBatchNumber;

	@XmlElement(name = "Communication_method", required = true)
	protected String communicationMethod;

	@XmlElement(name = "Year_of_manufactory", required = true)
	protected String yearOfManufactory;

	@XmlElement(name = "Hardware_version", required = true)
	protected String hardwareVersion;

	@XmlElement(name = "Core_active_firmware_version", required = true)
	protected String coreActiveFirmwareVersion;

	/**
	 * Obtient la valeur de la propri�t� serialNumber.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSerialNumber() {
		return serialNumber;
	}

	/**
	 * D�finit la valeur de la propri�t� serialNumber.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setSerialNumber(String value) {
		this.serialNumber = value;
	}

	/**
	 * Obtient la valeur de la propri�t� customerArticleNumber.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomerArticleNumber() {
		return customerArticleNumber;
	}

	/**
	 * D�finit la valeur de la propri�t� customerArticleNumber.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setCustomerArticleNumber(String value) {
		this.customerArticleNumber = value;
	}

	/**
	 * Obtient la valeur de la propri�t� customerLotNumber.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomerLotNumber() {
		return customerLotNumber;
	}

	/**
	 * D�finit la valeur de la propri�t� customerLotNumber.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setCustomerLotNumber(String value) {
		this.customerLotNumber = value;
	}

	/**
	 * Obtient la valeur de la propri�t� testReport.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTestReport() {
		return testReport;
	}

	/**
	 * D�finit la valeur de la propri�t� testReport.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setTestReport(String value) {
		this.testReport = value;
	}

	/**
	 * Obtient la valeur de la propri�t� certificationYear.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCertificationYear() {
		return certificationYear;
	}

	/**
	 * D�finit la valeur de la propri�t� certificationYear.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setCertificationYear(String value) {
		this.certificationYear = value;
	}

	/**
	 * Obtient la valeur de la propri�t� certificationNumberReport.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCertificationNumberReport() {
		return certificationNumberReport;
	}

	/**
	 * D�finit la valeur de la propri�t� certificationNumberReport.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setCertificationNumberReport(String value) {
		this.certificationNumberReport = value;
	}

	/**
	 * Obtient la valeur de la propri�t� deviceBatchNumber.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDeviceBatchNumber() {
		return deviceBatchNumber;
	}

	/**
	 * D�finit la valeur de la propri�t� deviceBatchNumber.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setDeviceBatchNumber(String value) {
		this.deviceBatchNumber = value;
	}

	/**
	 * Obtient la valeur de la propri�t� communicationMethod.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCommunicationMethod() {
		return communicationMethod;
	}

	/**
	 * D�finit la valeur de la propri�t� communicationMethod.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setCommunicationMethod(String value) {
		this.communicationMethod = value;
	}

	/**
	 * Obtient la valeur de la propri�t� yearOfManufactory.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getYearOfManufactory() {
		return yearOfManufactory;
	}

	/**
	 * D�finit la valeur de la propri�t� yearOfManufactory.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setYearOfManufactory(String value) {
		this.yearOfManufactory = value;
	}

	/**
	 * Obtient la valeur de la propri�t� hardwareVersion.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getHardwareVersion() {
		return hardwareVersion;
	}

	/**
	 * D�finit la valeur de la propri�t� hardwareVersion.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setHardwareVersion(String value) {
		this.hardwareVersion = value;
	}

	/**
	 * Obtient la valeur de la propri�t� coreActiveFirmwareVersion.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCoreActiveFirmwareVersion() {
		return coreActiveFirmwareVersion;
	}

	/**
	 * D�finit la valeur de la propri�t� coreActiveFirmwareVersion.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setCoreActiveFirmwareVersion(String value) {
		this.coreActiveFirmwareVersion = value;
	}

}
