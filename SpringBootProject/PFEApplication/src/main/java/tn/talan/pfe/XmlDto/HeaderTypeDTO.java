package tn.talan.pfe.XmlDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HeaderType", propOrder = { "deliveryNoteNumber", "deviceType", "configurationVersion", "shipmentDate",
		"orderId", "orderPositionId" })
public class HeaderTypeDTO {

	@XmlElement(name = "Delivery_Note_Number", required = true)
	protected String deliveryNoteNumber;
	@XmlElement(name = "Device_type", required = true)
	protected String deviceType;
	@XmlElement(name = "Configuration_version", required = true)
	protected String configurationVersion;
	@XmlElement(name = "Shipment_date", required = true)
	protected String shipmentDate;
	@XmlElement(name = "Order_id", required = true)
	protected String orderId;
	@XmlElement(name = "Order_position_id", required = true)
	protected String orderPositionId;

	/**
	 * Obtient la valeur de la propri�t� deliveryNoteNumber.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDeliveryNoteNumber() {
		return deliveryNoteNumber;
	}

	/**
	 * D�finit la valeur de la propri�t� deliveryNoteNumber.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setDeliveryNoteNumber(String value) {
		this.deliveryNoteNumber = value;
	}

	/**
	 * Obtient la valeur de la propri�t� deviceType.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDeviceType() {
		return deviceType;
	}

	/**
	 * D�finit la valeur de la propri�t� deviceType.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setDeviceType(String value) {
		this.deviceType = value;
	}

	/**
	 * Obtient la valeur de la propri�t� configurationVersion.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getConfigurationVersion() {
		return configurationVersion;
	}

	/**
	 * D�finit la valeur de la propri�t� configurationVersion.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setConfigurationVersion(String value) {
		this.configurationVersion = value;
	}

	/**
	 * Obtient la valeur de la propri�t� shipmentDate.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getShipmentDate() {
		return shipmentDate;
	}

	/**
	 * D�finit la valeur de la propri�t� shipmentDate.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setShipmentDate(String value) {
		this.shipmentDate = value;
	}

	/**
	 * Obtient la valeur de la propri�t� orderId.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * D�finit la valeur de la propri�t� orderId.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setOrderId(String value) {
		this.orderId = value;
	}

	/**
	 * Obtient la valeur de la propri�t� orderPositionId.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrderPositionId() {
		return orderPositionId;
	}

	/**
	 * D�finit la valeur de la propri�t� orderPositionId.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setOrderPositionId(String value) {
		this.orderPositionId = value;
	}

}
