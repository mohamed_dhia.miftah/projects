package tn.talan.pfe.XmlDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import tn.talan.xmlenc_.EncryptedDataType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MbusType", propOrder = { "individualMasterKey", "keyEncryptionKey", "firmwareAuthenticationKey",
		"localAuthenticationKey", "communicationIdGas" })
public class MbusTypeDTO {

	@XmlElement(name = "Individual_master_key", required = true)
	protected MbusTypeDTO.IndividualMasterKey individualMasterKey;
	@XmlElement(name = "Key_encryption_key", required = true)
	protected MbusTypeDTO.KeyEncryptionKey keyEncryptionKey;
	@XmlElement(name = "Firmware_authentication_key", required = true)
	protected MbusTypeDTO.FirmwareAuthenticationKey firmwareAuthenticationKey;
	@XmlElement(name = "Local_authentication_key", required = true)
	protected MbusTypeDTO.LocalAuthenticationKey localAuthenticationKey;
	@XmlElement(name = "Communication-id-Gas", required = true)
	protected CommunicationIdGasTypeDTO communicationIdGas;

	/**
	 * @return possible object is {@link MbusTypeDTO.IndividualMasterKey }
	 */
	public MbusTypeDTO.IndividualMasterKey getIndividualMasterKey() {
		return individualMasterKey;
	}

	/**
	 * @param value allowed object is {@link MbusTypeDTO.IndividualMasterKey }
	 */
	public void setIndividualMasterKey(MbusTypeDTO.IndividualMasterKey value) {
		this.individualMasterKey = value;
	}

	/**
	 * @return possible object is {@link MbusTypeDTO.KeyEncryptionKey }
	 */
	public MbusTypeDTO.KeyEncryptionKey getKeyEncryptionKey() {
		return keyEncryptionKey;
	}

	/**
	 * @param value allowed object is {@link MbusTypeDTO.KeyEncryptionKey }
	 */
	public void setKeyEncryptionKey(MbusTypeDTO.KeyEncryptionKey value) {
		this.keyEncryptionKey = value;
	}

	/**
	 * @return possible object is {@link MbusTypeDTO.FirmwareAuthenticationKey }
	 */
	public MbusTypeDTO.FirmwareAuthenticationKey getFirmwareAuthenticationKey() {
		return firmwareAuthenticationKey;
	}

	/**
	 * @param value allowed object is {@link MbusTypeDTO.FirmwareAuthenticationKey }
	 */
	public void setFirmwareAuthenticationKey(MbusTypeDTO.FirmwareAuthenticationKey value) {
		this.firmwareAuthenticationKey = value;
	}

	/**
	 * @return possible object is {@link MbusTypeDTO.LocalAuthenticationKey }
	 */
	public MbusTypeDTO.LocalAuthenticationKey getLocalAuthenticationKey() {
		return localAuthenticationKey;
	}

	/**
	 * @param value allowed object is {@link MbusTypeDTO.LocalAuthenticationKey }
	 */
	public void setLocalAuthenticationKey(MbusTypeDTO.LocalAuthenticationKey value) {
		this.localAuthenticationKey = value;
	}

	/**
	 * @return possible object is {@link CommunicationIdGasTypeDTO }
	 */
	public CommunicationIdGasTypeDTO getCommunicationIdGas() {
		return communicationIdGas;
	}

	/**
	 * @param value allowed object is {@link CommunicationIdGasTypeDTO }
	 */
	public void setCommunicationIdGas(CommunicationIdGasTypeDTO value) {
		this.communicationIdGas = value;
	}

	/*****************************************************************************************
	 **************************** class FirmwareAuthenticationKey ****************************
	 *****************************************************************************************/

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "encryptedData" })
	public static class FirmwareAuthenticationKey {

		@XmlElement(name = "EncryptedData", namespace = "http://www.w3.org/2001/04/xmlenc#", required = true)
		protected EncryptedDataType encryptedData;

		/**
		 * @return possible object is {@link EncryptedDataType }
		 */
		public EncryptedDataType getEncryptedData() {
			return encryptedData;
		}

		/**
		 * @param value allowed object is {@link EncryptedDataType }
		 */
		public void setEncryptedData(EncryptedDataType value) {
			this.encryptedData = value;
		}

	}

	/*****************************************************************************************
	 **************************** class IndividualMasterKey **********************************
	 *****************************************************************************************/

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "encryptedData" })
	public static class IndividualMasterKey {

		@XmlElement(name = "EncryptedData", namespace = "http://www.w3.org/2001/04/xmlenc#", required = true)
		protected EncryptedDataType encryptedData;

		/**
		 * @return possible object is {@link EncryptedDataType }
		 */
		public EncryptedDataType getEncryptedData() {
			return encryptedData;
		}

		/**
		 * @param value allowed object is {@link EncryptedDataType }
		 */
		public void setEncryptedData(EncryptedDataType value) {
			this.encryptedData = value;
		}

	}

	/*****************************************************************************************
	 **************************** class KeyEncryptionKey *************************************
	 *****************************************************************************************/
	
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "encryptedData" })
	public static class KeyEncryptionKey {

		@XmlElement(name = "EncryptedData", namespace = "http://www.w3.org/2001/04/xmlenc#", required = true)
		protected EncryptedDataType encryptedData;

		/**
		 * @return possible object is {@link EncryptedDataType }
		 */
		public EncryptedDataType getEncryptedData() {
			return encryptedData;
		}

		/**
		 * @param value allowed object is {@link EncryptedDataType }
		 */
		public void setEncryptedData(EncryptedDataType value) {
			this.encryptedData = value;
		}

	}

	/*****************************************************************************************
	 **************************** class LocalAuthenticationKey *******************************
	 *****************************************************************************************/
	
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "encryptedData" })
	public static class LocalAuthenticationKey {

		@XmlElement(name = "EncryptedData", namespace = "http://www.w3.org/2001/04/xmlenc#", required = true)
		protected EncryptedDataType encryptedData;

		/**
		 * @return possible object is {@link EncryptedDataType }
		 */
		public EncryptedDataType getEncryptedData() {
			return encryptedData;
		}

		/**
		 * @param value allowed object is {@link EncryptedDataType }
		 */
		public void setEncryptedData(EncryptedDataType value) {
			this.encryptedData = value;
		}

	}

}
