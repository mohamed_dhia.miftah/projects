package tn.talan.pfe.XmlDto;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the
 * com.sagemcom.shipmentfile.fluvius_shipment_file_meter_gaz_amm._2018_01
 * package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactoryDTO {

	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema
	 * derived classes for package:
	 * com.sagemcom.shipmentfile.fluvius_shipment_file_meter_gaz_amm._2018_01
	 * 
	 */
	public ObjectFactoryDTO() {
	}

	/**
	 * Create an instance of {@link ShipmentFileDTO }
	 * 
	 */
	public ShipmentFileDTO createShipmentFile() {
		return new ShipmentFileDTO();
	}

	/**
	 * Create an instance of {@link MbusTypeDTO }
	 * 
	 */
	public MbusTypeDTO createMbusType() {
		return new MbusTypeDTO();
	}

	/**
	 * Create an instance of {@link MainTypeDTO }
	 * 
	 */
	public MainTypeDTO createMainType() {
		return new MainTypeDTO();
	}

	/**
	 * Create an instance of {@link ShipmentFileDTO.Body }
	 * 
	 */
	public ShipmentFileDTO.Body createShipmentFileBody() {
		return new ShipmentFileDTO.Body();
	}

	/**
	 * Create an instance of {@link ShipmentFileDTO.Header }
	 * 
	 */
	public ShipmentFileDTO.Header createShipmentFileHeader() {
		return new ShipmentFileDTO.Header();
	}

	/**
	 * Create an instance of {@link ShipmentFileDTO.Header.Security }
	 * 
	 */
	public ShipmentFileDTO.Header.Security createShipmentFileHeaderSecurity() {
		return new ShipmentFileDTO.Header.Security();
	}

	/**
	 * Create an instance of {@link CargoBoxTypeDTO }
	 * 
	 */
	public CargoBoxTypeDTO createCargoBoxType() {
		return new CargoBoxTypeDTO();
	}

	/**
	 * Create an instance of {@link CommunicationIdGasTypeDTO }
	 * 
	 */
	public CommunicationIdGasTypeDTO createCommunicationIdGasType() {
		return new CommunicationIdGasTypeDTO();
	}

	/**
	 * Create an instance of {@link BatteryTypeDTO }
	 * 
	 */
	public BatteryTypeDTO createBatteryType() {
		return new BatteryTypeDTO();
	}

	/**
	 * Create an instance of {@link DLMSAttributesTypeDTO }
	 * 
	 */
	public DLMSAttributesTypeDTO createDLMSAttributesType() {
		return new DLMSAttributesTypeDTO();
	}

	/**
	 * Create an instance of {@link BatteriesTypeDTO }
	 * 
	 */
	public BatteriesTypeDTO createBatteriesType() {
		return new BatteriesTypeDTO();
	}

	/**
	 * Create an instance of {@link GeneralTypeDTO }
	 * 
	 */
	public GeneralTypeDTO createGeneralType() {
		return new GeneralTypeDTO();
	}

	/**
	 * Create an instance of {@link HeaderTypeDTO }
	 * 
	 */
	public HeaderTypeDTO createHeaderType() {
		return new HeaderTypeDTO();
	}

	/**
	 * Create an instance of {@link DeviceAttributesTypeDTO }
	 * 
	 */
	public DeviceAttributesTypeDTO createDeviceAttributesType() {
		return new DeviceAttributesTypeDTO();
	}

	/**
	 * Create an instance of {@link PalletTypeDTO }
	 * 
	 */
	public PalletTypeDTO createPalletType() {
		return new PalletTypeDTO();
	}

	/**
	 * Create an instance of {@link MbusTypeDTO.IndividualMasterKey }
	 * 
	 */
	public MbusTypeDTO.IndividualMasterKey createMbusTypeIndividualMasterKey() {
		return new MbusTypeDTO.IndividualMasterKey();
	}

	/**
	 * Create an instance of {@link MbusTypeDTO.KeyEncryptionKey }
	 * 
	 */
	public MbusTypeDTO.KeyEncryptionKey createMbusTypeKeyEncryptionKey() {
		return new MbusTypeDTO.KeyEncryptionKey();
	}

	/**
	 * Create an instance of {@link MbusTypeDTO.FirmwareAuthenticationKey }
	 * 
	 */
	public MbusTypeDTO.FirmwareAuthenticationKey createMbusTypeFirmwareAuthenticationKey() {
		return new MbusTypeDTO.FirmwareAuthenticationKey();
	}

	/**
	 * Create an instance of {@link MbusTypeDTO.LocalAuthenticationKey }
	 * 
	 */
	public MbusTypeDTO.LocalAuthenticationKey createMbusTypeLocalAuthenticationKey() {
		return new MbusTypeDTO.LocalAuthenticationKey();
	}

	/**
	 * Create an instance of {@link MainTypeDTO.PublicKeySiconia }
	 * 
	 */
	public MainTypeDTO.PublicKeySiconia createMainTypePublicKeySiconia() {
		return new MainTypeDTO.PublicKeySiconia();
	}

	/**
	 * Create an instance of {@link ShipmentFileDTO.Body.Shipment }
	 * 
	 */
	public ShipmentFileDTO.Body.Shipment createShipmentFileBodyShipment() {
		return new ShipmentFileDTO.Body.Shipment();
	}

	/**
	 * Create an instance of
	 * {@link ShipmentFileDTO.Header.Security.SecurityKeySiconia }
	 * 
	 */
	public ShipmentFileDTO.Header.Security.SecurityKeySiconia createShipmentFileHeaderSecuritySecurityKeySiconia() {
		return new ShipmentFileDTO.Header.Security.SecurityKeySiconia();
	}

}
