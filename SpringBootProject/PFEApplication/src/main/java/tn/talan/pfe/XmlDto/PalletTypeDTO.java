package tn.talan.pfe.XmlDto;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PalletType", propOrder = { "palletId", "box" })
public class PalletTypeDTO {

	@XmlElement(name = "Pallet_id", required = true)
	protected String palletId;
	@XmlElement(name = "Box", required = true)
	protected List<CargoBoxTypeDTO> box;

	/**
	 * @return possible object is {@link String }
	 */
	public String getPalletId() {
		return palletId;
	}

	/**
	 * @param value allowed object is {@link String }
	 */
	public void setPalletId(String value) {
		this.palletId = value;
	}

	/**
	 * Gets the value of the box property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot.
	 * Therefore any modification you make to the returned list will be present
	 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
	 * for the box property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getBox().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link CargoBoxTypeDTO
	 * }
	 * 
	 * 
	 */
	public List<CargoBoxTypeDTO> getBox() {
		if (box == null) {
			box = new ArrayList<CargoBoxTypeDTO>();
		}
		return this.box;
	}

}
