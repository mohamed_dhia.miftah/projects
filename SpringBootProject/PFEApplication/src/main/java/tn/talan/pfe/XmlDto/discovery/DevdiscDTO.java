//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 10:07:25 AM WAT 
//


package tn.talan.pfe.XmlDto.discovery;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "devId",
    "time",
    "vendor",
    "product",
    "hwVer",
    "swVer",
    "dcId",
    "subDevice"
})
@XmlRootElement(name = "devdisc")
public class DevdiscDTO {

    @XmlElement(required = true)
    protected String devId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar time;
    protected String vendor;
    protected String product;
    protected String hwVer;
    protected String swVer;
    protected String dcId;
    protected DevdiscDTO.SubDevice subDevice;

    /**
     * Obtient la valeur de la propri�t� devId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDevId() {
        return devId;
    }

    /**
     * D�finit la valeur de la propri�t� devId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDevId(String value) {
        this.devId = value;
    }

    /**
     * Obtient la valeur de la propri�t� time.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTime() {
        return time;
    }

    /**
     * D�finit la valeur de la propri�t� time.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTime(XMLGregorianCalendar value) {
        this.time = value;
    }

    /**
     * Obtient la valeur de la propri�t� vendor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendor() {
        return vendor;
    }

    /**
     * D�finit la valeur de la propri�t� vendor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendor(String value) {
        this.vendor = value;
    }

    /**
     * Obtient la valeur de la propri�t� product.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProduct() {
        return product;
    }

    /**
     * D�finit la valeur de la propri�t� product.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProduct(String value) {
        this.product = value;
    }

    /**
     * Obtient la valeur de la propri�t� hwVer.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHwVer() {
        return hwVer;
    }

    /**
     * D�finit la valeur de la propri�t� hwVer.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHwVer(String value) {
        this.hwVer = value;
    }

    /**
     * Obtient la valeur de la propri�t� swVer.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSwVer() {
        return swVer;
    }

    /**
     * D�finit la valeur de la propri�t� swVer.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSwVer(String value) {
        this.swVer = value;
    }

    /**
     * Obtient la valeur de la propri�t� dcId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDcId() {
        return dcId;
    }

    /**
     * D�finit la valeur de la propri�t� dcId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDcId(String value) {
        this.dcId = value;
    }

    /**
     * Obtient la valeur de la propri�t� subDevice.
     * 
     * @return
     *     possible object is
     *     {@link DevdiscDTO.SubDevice }
     *     
     */
    public DevdiscDTO.SubDevice getSubDevice() {
        return subDevice;
    }

    /**
     * D�finit la valeur de la propri�t� subDevice.
     * 
     * @param value
     *     allowed object is
     *     {@link DevdiscDTO.SubDevice }
     *     
     */
    public void setSubDevice(DevdiscDTO.SubDevice value) {
        this.subDevice = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="netType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="devId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="channel" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="param" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "netType",
        "devId",
        "channel",
        "param"
    })
    public static class SubDevice {

        @XmlElement(required = true)
        protected String netType;
        @XmlElement(required = true)
        protected String devId;
        protected BigInteger channel;
        protected DevdiscDTO.SubDevice.Param param;

        /**
         * Obtient la valeur de la propri�t� netType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNetType() {
            return netType;
        }

        /**
         * D�finit la valeur de la propri�t� netType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNetType(String value) {
            this.netType = value;
        }

        /**
         * Obtient la valeur de la propri�t� devId.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDevId() {
            return devId;
        }

        /**
         * D�finit la valeur de la propri�t� devId.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDevId(String value) {
            this.devId = value;
        }

        /**
         * Obtient la valeur de la propri�t� channel.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getChannel() {
            return channel;
        }

        /**
         * D�finit la valeur de la propri�t� channel.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setChannel(BigInteger value) {
            this.channel = value;
        }

        /**
         * Obtient la valeur de la propri�t� param.
         * 
         * @return
         *     possible object is
         *     {@link DevdiscDTO.SubDevice.Param }
         *     
         */
        public DevdiscDTO.SubDevice.Param getParam() {
            return param;
        }

        /**
         * D�finit la valeur de la propri�t� param.
         * 
         * @param value
         *     allowed object is
         *     {@link DevdiscDTO.SubDevice.Param }
         *     
         */
        public void setParam(DevdiscDTO.SubDevice.Param value) {
            this.param = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         * 
         * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "name",
            "value"
        })
        public static class Param {

            @XmlElement(required = true)
            protected String name;
            @XmlElement(required = true)
            protected String value;

            /**
             * Obtient la valeur de la propri�t� name.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * D�finit la valeur de la propri�t� name.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Obtient la valeur de la propri�t� value.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * D�finit la valeur de la propri�t� value.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

        }

    }

}
