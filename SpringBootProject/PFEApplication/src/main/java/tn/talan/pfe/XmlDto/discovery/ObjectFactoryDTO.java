//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 10:07:25 AM WAT 
//


package tn.talan.pfe.XmlDto.discovery;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sagemcom.amm.hes2015.types.devicedisc.v2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactoryDTO {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sagemcom.amm.hes2015.types.devicedisc.v2
     * 
     */
    public ObjectFactoryDTO() {
    }

    /**
     * Create an instance of {@link DevdiscDTO }
     * 
     */
    public DevdiscDTO createDevdisc() {
        return new DevdiscDTO();
    }

    /**
     * Create an instance of {@link DevdiscDTO.SubDevice }
     * 
     */
    public DevdiscDTO.SubDevice createDevdiscSubDevice() {
        return new DevdiscDTO.SubDevice();
    }

    /**
     * Create an instance of {@link DevdiscDTO.SubDevice.Param }
     * 
     */
    public DevdiscDTO.SubDevice.Param createDevdiscSubDeviceParam() {
        return new DevdiscDTO.SubDevice.Param();
    }

}
