//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour CheckMeterConfigurationReplyHeader complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="CheckMeterConfigurationReplyHeader">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.emeter.com/energyip/amiinterface}ReplyHeader">
 *       &lt;sequence>
 *         &lt;element name="verificationDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="isVerified" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="additionalInfo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="measurementProfile" type="{http://www.emeter.com/energyip/amiinterface}ConfigurationProfileStatus" minOccurs="0"/>
 *         &lt;element name="touProfile" type="{http://www.emeter.com/energyip/amiinterface}ConfigurationProfileStatus" minOccurs="0"/>
 *         &lt;element name="holidayProfile" type="{http://www.emeter.com/energyip/amiinterface}ConfigurationProfileStatus" minOccurs="0"/>
 *         &lt;element name="loadLimitProfile" type="{http://www.emeter.com/energyip/amiinterface}ConfigurationProfileStatus" minOccurs="0"/>
 *         &lt;element name="loadControlTimeSwitchProfile" type="{http://www.emeter.com/energyip/amiinterface}ConfigurationProfileStatus" minOccurs="0"/>
 *         &lt;element name="issuerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="issuerTrackingId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheckMeterConfigurationReplyHeader", propOrder = {
    "verificationDateTime",
    "isVerified",
    "additionalInfo",
    "measurementProfile",
    "touProfile",
    "holidayProfile",
    "loadLimitProfile",
    "loadControlTimeSwitchProfile",
    "issuerId",
    "issuerTrackingId",
    "reason"
})
public class CheckMeterConfigurationReplyHeader
    extends ReplyHeader
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar verificationDateTime;
    protected boolean isVerified;
    @XmlElement(required = true)
    protected String additionalInfo;
    protected ConfigurationProfileStatus measurementProfile;
    protected ConfigurationProfileStatus touProfile;
    protected ConfigurationProfileStatus holidayProfile;
    protected ConfigurationProfileStatus loadLimitProfile;
    protected ConfigurationProfileStatus loadControlTimeSwitchProfile;
    protected String issuerId;
    protected String issuerTrackingId;
    protected String reason;

    /**
     * Obtient la valeur de la propri�t� verificationDateTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVerificationDateTime() {
        return verificationDateTime;
    }

    /**
     * D�finit la valeur de la propri�t� verificationDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVerificationDateTime(XMLGregorianCalendar value) {
        this.verificationDateTime = value;
    }

    /**
     * Obtient la valeur de la propri�t� isVerified.
     * 
     */
    public boolean isIsVerified() {
        return isVerified;
    }

    /**
     * D�finit la valeur de la propri�t� isVerified.
     * 
     */
    public void setIsVerified(boolean value) {
        this.isVerified = value;
    }

    /**
     * Obtient la valeur de la propri�t� additionalInfo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalInfo() {
        return additionalInfo;
    }

    /**
     * D�finit la valeur de la propri�t� additionalInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalInfo(String value) {
        this.additionalInfo = value;
    }

    /**
     * Obtient la valeur de la propri�t� measurementProfile.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationProfileStatus }
     *     
     */
    public ConfigurationProfileStatus getMeasurementProfile() {
        return measurementProfile;
    }

    /**
     * D�finit la valeur de la propri�t� measurementProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationProfileStatus }
     *     
     */
    public void setMeasurementProfile(ConfigurationProfileStatus value) {
        this.measurementProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� touProfile.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationProfileStatus }
     *     
     */
    public ConfigurationProfileStatus getTouProfile() {
        return touProfile;
    }

    /**
     * D�finit la valeur de la propri�t� touProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationProfileStatus }
     *     
     */
    public void setTouProfile(ConfigurationProfileStatus value) {
        this.touProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� holidayProfile.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationProfileStatus }
     *     
     */
    public ConfigurationProfileStatus getHolidayProfile() {
        return holidayProfile;
    }

    /**
     * D�finit la valeur de la propri�t� holidayProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationProfileStatus }
     *     
     */
    public void setHolidayProfile(ConfigurationProfileStatus value) {
        this.holidayProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� loadLimitProfile.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationProfileStatus }
     *     
     */
    public ConfigurationProfileStatus getLoadLimitProfile() {
        return loadLimitProfile;
    }

    /**
     * D�finit la valeur de la propri�t� loadLimitProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationProfileStatus }
     *     
     */
    public void setLoadLimitProfile(ConfigurationProfileStatus value) {
        this.loadLimitProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� loadControlTimeSwitchProfile.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationProfileStatus }
     *     
     */
    public ConfigurationProfileStatus getLoadControlTimeSwitchProfile() {
        return loadControlTimeSwitchProfile;
    }

    /**
     * D�finit la valeur de la propri�t� loadControlTimeSwitchProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationProfileStatus }
     *     
     */
    public void setLoadControlTimeSwitchProfile(ConfigurationProfileStatus value) {
        this.loadControlTimeSwitchProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� issuerId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerId() {
        return issuerId;
    }

    /**
     * D�finit la valeur de la propri�t� issuerId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerId(String value) {
        this.issuerId = value;
    }

    /**
     * Obtient la valeur de la propri�t� issuerTrackingId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerTrackingId() {
        return issuerTrackingId;
    }

    /**
     * D�finit la valeur de la propri�t� issuerTrackingId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerTrackingId(String value) {
        this.issuerTrackingId = value;
    }

    /**
     * Obtient la valeur de la propri�t� reason.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * D�finit la valeur de la propri�t� reason.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
