//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour CheckMeterConfigurationRequestPayload complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="CheckMeterConfigurationRequestPayload">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="meterAsset" type="{http://www.emeter.com/energyip/amiinterface}MeterAsset"/>
 *         &lt;element name="requestPriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="measurementProfile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="touProfile" type="{http://www.emeter.com/energyip/amiinterface}ConfigurationProfile" minOccurs="0"/>
 *         &lt;element name="holidayProfile" type="{http://www.emeter.com/energyip/amiinterface}ConfigurationProfile" minOccurs="0"/>
 *         &lt;element name="loadLimitProfile" type="{http://www.emeter.com/energyip/amiinterface}ConfigurationProfile" minOccurs="0"/>
 *         &lt;element name="loadControlTimeSwitchProfile" type="{http://www.emeter.com/energyip/amiinterface}ConfigurationProfile" minOccurs="0"/>
 *         &lt;element name="issuerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="issuerTrackingId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheckMeterConfigurationRequestPayload", propOrder = {
    "meterAsset",
    "requestPriority",
    "measurementProfile",
    "touProfile",
    "holidayProfile",
    "loadLimitProfile",
    "loadControlTimeSwitchProfile",
    "issuerId",
    "issuerTrackingId",
    "reason"
})
public class CheckMeterConfigurationRequestPayload {

    @XmlElement(required = true)
    protected MeterAsset meterAsset;
    protected String requestPriority;
    protected String measurementProfile;
    protected ConfigurationProfile touProfile;
    protected ConfigurationProfile holidayProfile;
    protected ConfigurationProfile loadLimitProfile;
    protected ConfigurationProfile loadControlTimeSwitchProfile;
    protected String issuerId;
    protected String issuerTrackingId;
    protected String reason;

    /**
     * Obtient la valeur de la propri�t� meterAsset.
     * 
     * @return
     *     possible object is
     *     {@link MeterAsset }
     *     
     */
    public MeterAsset getMeterAsset() {
        return meterAsset;
    }

    /**
     * D�finit la valeur de la propri�t� meterAsset.
     * 
     * @param value
     *     allowed object is
     *     {@link MeterAsset }
     *     
     */
    public void setMeterAsset(MeterAsset value) {
        this.meterAsset = value;
    }

    /**
     * Obtient la valeur de la propri�t� requestPriority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestPriority() {
        return requestPriority;
    }

    /**
     * D�finit la valeur de la propri�t� requestPriority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestPriority(String value) {
        this.requestPriority = value;
    }

    /**
     * Obtient la valeur de la propri�t� measurementProfile.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasurementProfile() {
        return measurementProfile;
    }

    /**
     * D�finit la valeur de la propri�t� measurementProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasurementProfile(String value) {
        this.measurementProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� touProfile.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationProfile }
     *     
     */
    public ConfigurationProfile getTouProfile() {
        return touProfile;
    }

    /**
     * D�finit la valeur de la propri�t� touProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationProfile }
     *     
     */
    public void setTouProfile(ConfigurationProfile value) {
        this.touProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� holidayProfile.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationProfile }
     *     
     */
    public ConfigurationProfile getHolidayProfile() {
        return holidayProfile;
    }

    /**
     * D�finit la valeur de la propri�t� holidayProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationProfile }
     *     
     */
    public void setHolidayProfile(ConfigurationProfile value) {
        this.holidayProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� loadLimitProfile.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationProfile }
     *     
     */
    public ConfigurationProfile getLoadLimitProfile() {
        return loadLimitProfile;
    }

    /**
     * D�finit la valeur de la propri�t� loadLimitProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationProfile }
     *     
     */
    public void setLoadLimitProfile(ConfigurationProfile value) {
        this.loadLimitProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� loadControlTimeSwitchProfile.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationProfile }
     *     
     */
    public ConfigurationProfile getLoadControlTimeSwitchProfile() {
        return loadControlTimeSwitchProfile;
    }

    /**
     * D�finit la valeur de la propri�t� loadControlTimeSwitchProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationProfile }
     *     
     */
    public void setLoadControlTimeSwitchProfile(ConfigurationProfile value) {
        this.loadControlTimeSwitchProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� issuerId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerId() {
        return issuerId;
    }

    /**
     * D�finit la valeur de la propri�t� issuerId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerId(String value) {
        this.issuerId = value;
    }

    /**
     * Obtient la valeur de la propri�t� issuerTrackingId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerTrackingId() {
        return issuerTrackingId;
    }

    /**
     * D�finit la valeur de la propri�t� issuerTrackingId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerTrackingId(String value) {
        this.issuerTrackingId = value;
    }

    /**
     * Obtient la valeur de la propri�t� reason.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * D�finit la valeur de la propri�t� reason.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
