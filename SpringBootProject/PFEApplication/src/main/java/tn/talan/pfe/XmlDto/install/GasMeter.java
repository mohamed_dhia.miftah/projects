//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour GasMeter complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="GasMeter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="operatingPressure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flowRange" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GasMeter", propOrder = {
    "operatingPressure",
    "flowRange"
})
public class GasMeter {

    protected String operatingPressure;
    protected String flowRange;

    /**
     * Obtient la valeur de la propri�t� operatingPressure.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatingPressure() {
        return operatingPressure;
    }

    /**
     * D�finit la valeur de la propri�t� operatingPressure.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatingPressure(String value) {
        this.operatingPressure = value;
    }

    /**
     * Obtient la valeur de la propri�t� flowRange.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlowRange() {
        return flowRange;
    }

    /**
     * D�finit la valeur de la propri�t� flowRange.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlowRange(String value) {
        this.flowRange = value;
    }

}
