//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour IReading complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="IReading">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="startTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="endTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="intervalLength" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="demandPeakTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="measurementSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flags" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="collectionTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="quality" type="{http://www.emeter.com/energyip/amiinterface}Quality" minOccurs="0"/>
 *         &lt;element name="amiRecordNum" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="reasonForRead" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IReading", propOrder = {
    "startTime",
    "endTime",
    "intervalLength",
    "value",
    "demandPeakTime",
    "measurementSource",
    "flags",
    "status",
    "collectionTime",
    "quality",
    "amiRecordNum",
    "reasonForRead"
})
@XmlSeeAlso({
    Reading.class
})
public class IReading {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startTime;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endTime;
    protected Integer intervalLength;
    protected Double value;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar demandPeakTime;
    protected String measurementSource;
    protected Long flags;
    protected Long status;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar collectionTime;
    protected Quality quality;
    protected Long amiRecordNum;
    protected String reasonForRead;

    /**
     * Obtient la valeur de la propri�t� startTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartTime() {
        return startTime;
    }

    /**
     * D�finit la valeur de la propri�t� startTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartTime(XMLGregorianCalendar value) {
        this.startTime = value;
    }

    /**
     * Obtient la valeur de la propri�t� endTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndTime() {
        return endTime;
    }

    /**
     * D�finit la valeur de la propri�t� endTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndTime(XMLGregorianCalendar value) {
        this.endTime = value;
    }

    /**
     * Obtient la valeur de la propri�t� intervalLength.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIntervalLength() {
        return intervalLength;
    }

    /**
     * D�finit la valeur de la propri�t� intervalLength.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIntervalLength(Integer value) {
        this.intervalLength = value;
    }

    /**
     * Obtient la valeur de la propri�t� value.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getValue() {
        return value;
    }

    /**
     * D�finit la valeur de la propri�t� value.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setValue(Double value) {
        this.value = value;
    }

    /**
     * Obtient la valeur de la propri�t� demandPeakTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDemandPeakTime() {
        return demandPeakTime;
    }

    /**
     * D�finit la valeur de la propri�t� demandPeakTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDemandPeakTime(XMLGregorianCalendar value) {
        this.demandPeakTime = value;
    }

    /**
     * Obtient la valeur de la propri�t� measurementSource.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasurementSource() {
        return measurementSource;
    }

    /**
     * D�finit la valeur de la propri�t� measurementSource.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasurementSource(String value) {
        this.measurementSource = value;
    }

    /**
     * Obtient la valeur de la propri�t� flags.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getFlags() {
        return flags;
    }

    /**
     * D�finit la valeur de la propri�t� flags.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setFlags(Long value) {
        this.flags = value;
    }

    /**
     * Obtient la valeur de la propri�t� status.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getStatus() {
        return status;
    }

    /**
     * D�finit la valeur de la propri�t� status.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setStatus(Long value) {
        this.status = value;
    }

    /**
     * Obtient la valeur de la propri�t� collectionTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCollectionTime() {
        return collectionTime;
    }

    /**
     * D�finit la valeur de la propri�t� collectionTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCollectionTime(XMLGregorianCalendar value) {
        this.collectionTime = value;
    }

    /**
     * Obtient la valeur de la propri�t� quality.
     * 
     * @return
     *     possible object is
     *     {@link Quality }
     *     
     */
    public Quality getQuality() {
        return quality;
    }

    /**
     * D�finit la valeur de la propri�t� quality.
     * 
     * @param value
     *     allowed object is
     *     {@link Quality }
     *     
     */
    public void setQuality(Quality value) {
        this.quality = value;
    }

    /**
     * Obtient la valeur de la propri�t� amiRecordNum.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAmiRecordNum() {
        return amiRecordNum;
    }

    /**
     * D�finit la valeur de la propri�t� amiRecordNum.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAmiRecordNum(Long value) {
        this.amiRecordNum = value;
    }

    /**
     * Obtient la valeur de la propri�t� reasonForRead.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonForRead() {
        return reasonForRead;
    }

    /**
     * D�finit la valeur de la propri�t� reasonForRead.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonForRead(String value) {
        this.reasonForRead = value;
    }

}
