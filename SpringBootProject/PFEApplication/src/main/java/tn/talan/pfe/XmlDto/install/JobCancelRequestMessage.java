//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="header" type="{http://www.emeter.com/energyip/amiinterface}MessageHeader"/>
 *         &lt;element name="payload" type="{http://www.emeter.com/energyip/amiinterface}JobCancelRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "payload"
})
@XmlRootElement(name = "JobCancelRequestMessage")
public class JobCancelRequestMessage {

    @XmlElement(required = true)
    protected MessageHeader header;
    protected JobCancelRequest payload;

    /**
     * Obtient la valeur de la propri�t� header.
     * 
     * @return
     *     possible object is
     *     {@link MessageHeader }
     *     
     */
    public MessageHeader getHeader() {
        return header;
    }

    /**
     * D�finit la valeur de la propri�t� header.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageHeader }
     *     
     */
    public void setHeader(MessageHeader value) {
        this.header = value;
    }

    /**
     * Obtient la valeur de la propri�t� payload.
     * 
     * @return
     *     possible object is
     *     {@link JobCancelRequest }
     *     
     */
    public JobCancelRequest getPayload() {
        return payload;
    }

    /**
     * D�finit la valeur de la propri�t� payload.
     * 
     * @param value
     *     allowed object is
     *     {@link JobCancelRequest }
     *     
     */
    public void setPayload(JobCancelRequest value) {
        this.payload = value;
    }

}
