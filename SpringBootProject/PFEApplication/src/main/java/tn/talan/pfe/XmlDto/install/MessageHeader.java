//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Message header containing descriptive information
 * 				about the message.
 * 
 * <p>Classe Java pour MessageHeader complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="MessageHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="verb" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="cancel"/>
 *               &lt;enumeration value="canceled"/>
 *               &lt;enumeration value="change"/>
 *               &lt;enumeration value="changed"/>
 *               &lt;enumeration value="create"/>
 *               &lt;enumeration value="created"/>
 *               &lt;enumeration value="close"/>
 *               &lt;enumeration value="closed"/>
 *               &lt;enumeration value="delete"/>
 *               &lt;enumeration value="deleted"/>
 *               &lt;enumeration value="get"/>
 *               &lt;enumeration value="show"/>
 *               &lt;enumeration value="request"/>
 *               &lt;enumeration value="reply"/>
 *               &lt;enumeration value="ack"/>
 *               &lt;enumeration value="subscribe"/>
 *               &lt;enumeration value="unsubscribe"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="noun" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="revision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="source" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="messageID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="asyncReplyTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="utilityId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageHeader", propOrder = {
    "verb",
    "noun",
    "revision",
    "dateTime",
    "source",
    "messageID",
    "asyncReplyTo",
    "utilityId"
})
public class MessageHeader {

    protected String verb;
    protected String noun;
    @XmlElement(defaultValue = "1")
    protected String revision;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateTime;
    protected String source;
    protected String messageID;
    protected String asyncReplyTo;
    protected String utilityId;

    /**
     * Obtient la valeur de la propri�t� verb.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerb() {
        return verb;
    }

    /**
     * D�finit la valeur de la propri�t� verb.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerb(String value) {
        this.verb = value;
    }

    /**
     * Obtient la valeur de la propri�t� noun.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoun() {
        return noun;
    }

    /**
     * D�finit la valeur de la propri�t� noun.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoun(String value) {
        this.noun = value;
    }

    /**
     * Obtient la valeur de la propri�t� revision.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRevision() {
        return revision;
    }

    /**
     * D�finit la valeur de la propri�t� revision.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRevision(String value) {
        this.revision = value;
    }

    /**
     * Obtient la valeur de la propri�t� dateTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateTime() {
        return dateTime;
    }

    /**
     * D�finit la valeur de la propri�t� dateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateTime(XMLGregorianCalendar value) {
        this.dateTime = value;
    }

    /**
     * Obtient la valeur de la propri�t� source.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSource() {
        return source;
    }

    /**
     * D�finit la valeur de la propri�t� source.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSource(String value) {
        this.source = value;
    }

    /**
     * Obtient la valeur de la propri�t� messageID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageID() {
        return messageID;
    }

    /**
     * D�finit la valeur de la propri�t� messageID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageID(String value) {
        this.messageID = value;
    }

    /**
     * Obtient la valeur de la propri�t� asyncReplyTo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAsyncReplyTo() {
        return asyncReplyTo;
    }

    /**
     * D�finit la valeur de la propri�t� asyncReplyTo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAsyncReplyTo(String value) {
        this.asyncReplyTo = value;
    }

    /**
     * Obtient la valeur de la propri�t� utilityId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUtilityId() {
        return utilityId;
    }

    /**
     * D�finit la valeur de la propri�t� utilityId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUtilityId(String value) {
        this.utilityId = value;
    }

}
