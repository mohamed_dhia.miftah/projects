//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour MeterAsset complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="MeterAsset">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.emeter.com/energyip/amiinterface}Asset">
 *       &lt;sequence>
 *         &lt;element name="serviceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="electricMeter" type="{http://www.emeter.com/energyip/amiinterface}ElectricMeter" minOccurs="0"/>
 *           &lt;element name="waterMeter" type="{http://www.emeter.com/energyip/amiinterface}WaterMeter" minOccurs="0"/>
 *           &lt;element name="gasMeter" type="{http://www.emeter.com/energyip/amiinterface}GasMeter" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="numDigits" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="typeMeter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProgramId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeterAsset", propOrder = {
    "serviceType",
    "electricMeter",
    "waterMeter",
    "gasMeter",
    "numDigits",
    "typeMeter",
    "programId"
})
public class MeterAsset
    extends Asset
{

    protected String serviceType;
    protected ElectricMeter electricMeter;
    protected WaterMeter waterMeter;
    protected GasMeter gasMeter;
    protected BigInteger numDigits;
    protected String typeMeter;
    @XmlElement(name = "ProgramId")
    protected String programId;

    /**
     * Obtient la valeur de la propri�t� serviceType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * D�finit la valeur de la propri�t� serviceType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceType(String value) {
        this.serviceType = value;
    }

    /**
     * Obtient la valeur de la propri�t� electricMeter.
     * 
     * @return
     *     possible object is
     *     {@link ElectricMeter }
     *     
     */
    public ElectricMeter getElectricMeter() {
        return electricMeter;
    }

    /**
     * D�finit la valeur de la propri�t� electricMeter.
     * 
     * @param value
     *     allowed object is
     *     {@link ElectricMeter }
     *     
     */
    public void setElectricMeter(ElectricMeter value) {
        this.electricMeter = value;
    }

    /**
     * Obtient la valeur de la propri�t� waterMeter.
     * 
     * @return
     *     possible object is
     *     {@link WaterMeter }
     *     
     */
    public WaterMeter getWaterMeter() {
        return waterMeter;
    }

    /**
     * D�finit la valeur de la propri�t� waterMeter.
     * 
     * @param value
     *     allowed object is
     *     {@link WaterMeter }
     *     
     */
    public void setWaterMeter(WaterMeter value) {
        this.waterMeter = value;
    }

    /**
     * Obtient la valeur de la propri�t� gasMeter.
     * 
     * @return
     *     possible object is
     *     {@link GasMeter }
     *     
     */
    public GasMeter getGasMeter() {
        return gasMeter;
    }

    /**
     * D�finit la valeur de la propri�t� gasMeter.
     * 
     * @param value
     *     allowed object is
     *     {@link GasMeter }
     *     
     */
    public void setGasMeter(GasMeter value) {
        this.gasMeter = value;
    }

    /**
     * Obtient la valeur de la propri�t� numDigits.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumDigits() {
        return numDigits;
    }

    /**
     * D�finit la valeur de la propri�t� numDigits.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumDigits(BigInteger value) {
        this.numDigits = value;
    }

    /**
     * Obtient la valeur de la propri�t� typeMeter.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeMeter() {
        return typeMeter;
    }

    /**
     * D�finit la valeur de la propri�t� typeMeter.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeMeter(String value) {
        this.typeMeter = value;
    }

    /**
     * Obtient la valeur de la propri�t� programId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramId() {
        return programId;
    }

    /**
     * D�finit la valeur de la propri�t� programId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramId(String value) {
        this.programId = value;
    }

}
