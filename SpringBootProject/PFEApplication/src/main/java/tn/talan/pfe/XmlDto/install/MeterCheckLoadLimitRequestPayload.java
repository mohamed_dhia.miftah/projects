//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour MeterCheckLoadLimitRequestPayload complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="MeterCheckLoadLimitRequestPayload">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="meterAsset" type="{http://www.emeter.com/energyip/amiinterface}MeterAsset"/>
 *         &lt;element name="requestPriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="loadLimitProfile" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="hasLoadLimitProfile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="issuerId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="issuerTrackingId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeterCheckLoadLimitRequestPayload", propOrder = {
    "meterAsset",
    "requestPriority",
    "loadLimitProfile",
    "hasLoadLimitProfile",
    "issuerId",
    "issuerTrackingId",
    "reason"
})
public class MeterCheckLoadLimitRequestPayload {

    @XmlElement(required = true)
    protected MeterAsset meterAsset;
    protected String requestPriority;
    @XmlElement(required = true)
    protected String loadLimitProfile;
    protected boolean hasLoadLimitProfile;
    @XmlElement(required = true)
    protected String issuerId;
    @XmlElement(required = true)
    protected String issuerTrackingId;
    @XmlElement(required = true)
    protected String reason;

    /**
     * Obtient la valeur de la propri�t� meterAsset.
     * 
     * @return
     *     possible object is
     *     {@link MeterAsset }
     *     
     */
    public MeterAsset getMeterAsset() {
        return meterAsset;
    }

    /**
     * D�finit la valeur de la propri�t� meterAsset.
     * 
     * @param value
     *     allowed object is
     *     {@link MeterAsset }
     *     
     */
    public void setMeterAsset(MeterAsset value) {
        this.meterAsset = value;
    }

    /**
     * Obtient la valeur de la propri�t� requestPriority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestPriority() {
        return requestPriority;
    }

    /**
     * D�finit la valeur de la propri�t� requestPriority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestPriority(String value) {
        this.requestPriority = value;
    }

    /**
     * Obtient la valeur de la propri�t� loadLimitProfile.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoadLimitProfile() {
        return loadLimitProfile;
    }

    /**
     * D�finit la valeur de la propri�t� loadLimitProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoadLimitProfile(String value) {
        this.loadLimitProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� hasLoadLimitProfile.
     * 
     */
    public boolean isHasLoadLimitProfile() {
        return hasLoadLimitProfile;
    }

    /**
     * D�finit la valeur de la propri�t� hasLoadLimitProfile.
     * 
     */
    public void setHasLoadLimitProfile(boolean value) {
        this.hasLoadLimitProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� issuerId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerId() {
        return issuerId;
    }

    /**
     * D�finit la valeur de la propri�t� issuerId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerId(String value) {
        this.issuerId = value;
    }

    /**
     * Obtient la valeur de la propri�t� issuerTrackingId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerTrackingId() {
        return issuerTrackingId;
    }

    /**
     * D�finit la valeur de la propri�t� issuerTrackingId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerTrackingId(String value) {
        this.issuerTrackingId = value;
    }

    /**
     * Obtient la valeur de la propri�t� reason.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * D�finit la valeur de la propri�t� reason.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
