//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour MeterDataRequest complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="MeterDataRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="startTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="endTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="versionTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Meter" type="{http://www.emeter.com/energyip/amiinterface}MeterAsset"/>
 *         &lt;element name="measurementProfile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="readingTypeList" type="{http://www.emeter.com/energyip/amiinterface}ReadingTypeList" minOccurs="0"/>
 *         &lt;element name="eventCategory" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="retrieveRegisterReads" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="retrieveIntervalReads" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="demandReset" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="retrieveEvents" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="requestPriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="executeStartTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="executeExpireTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeterDataRequest", propOrder = {
    "startTime",
    "endTime",
    "versionTime",
    "meter",
    "measurementProfile",
    "readingTypeList",
    "eventCategory",
    "retrieveRegisterReads",
    "retrieveIntervalReads",
    "demandReset",
    "retrieveEvents",
    "requestPriority",
    "executeStartTime",
    "executeExpireTime"
})
public class MeterDataRequest {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startTime;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endTime;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar versionTime;
    @XmlElement(name = "Meter", required = true)
    protected MeterAsset meter;
    protected String measurementProfile;
    protected ReadingTypeList readingTypeList;
    protected List<String> eventCategory;
    protected Boolean retrieveRegisterReads;
    protected Boolean retrieveIntervalReads;
    protected Boolean demandReset;
    protected Boolean retrieveEvents;
    protected String requestPriority;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar executeStartTime;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar executeExpireTime;

    /**
     * Obtient la valeur de la propri�t� startTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartTime() {
        return startTime;
    }

    /**
     * D�finit la valeur de la propri�t� startTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartTime(XMLGregorianCalendar value) {
        this.startTime = value;
    }

    /**
     * Obtient la valeur de la propri�t� endTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndTime() {
        return endTime;
    }

    /**
     * D�finit la valeur de la propri�t� endTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndTime(XMLGregorianCalendar value) {
        this.endTime = value;
    }

    /**
     * Obtient la valeur de la propri�t� versionTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVersionTime() {
        return versionTime;
    }

    /**
     * D�finit la valeur de la propri�t� versionTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVersionTime(XMLGregorianCalendar value) {
        this.versionTime = value;
    }

    /**
     * Obtient la valeur de la propri�t� meter.
     * 
     * @return
     *     possible object is
     *     {@link MeterAsset }
     *     
     */
    public MeterAsset getMeter() {
        return meter;
    }

    /**
     * D�finit la valeur de la propri�t� meter.
     * 
     * @param value
     *     allowed object is
     *     {@link MeterAsset }
     *     
     */
    public void setMeter(MeterAsset value) {
        this.meter = value;
    }

    /**
     * Obtient la valeur de la propri�t� measurementProfile.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasurementProfile() {
        return measurementProfile;
    }

    /**
     * D�finit la valeur de la propri�t� measurementProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasurementProfile(String value) {
        this.measurementProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� readingTypeList.
     * 
     * @return
     *     possible object is
     *     {@link ReadingTypeList }
     *     
     */
    public ReadingTypeList getReadingTypeList() {
        return readingTypeList;
    }

    /**
     * D�finit la valeur de la propri�t� readingTypeList.
     * 
     * @param value
     *     allowed object is
     *     {@link ReadingTypeList }
     *     
     */
    public void setReadingTypeList(ReadingTypeList value) {
        this.readingTypeList = value;
    }

    /**
     * Gets the value of the eventCategory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eventCategory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEventCategory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getEventCategory() {
        if (eventCategory == null) {
            eventCategory = new ArrayList<String>();
        }
        return this.eventCategory;
    }

    /**
     * Obtient la valeur de la propri�t� retrieveRegisterReads.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetrieveRegisterReads() {
        return retrieveRegisterReads;
    }

    /**
     * D�finit la valeur de la propri�t� retrieveRegisterReads.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetrieveRegisterReads(Boolean value) {
        this.retrieveRegisterReads = value;
    }

    /**
     * Obtient la valeur de la propri�t� retrieveIntervalReads.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetrieveIntervalReads() {
        return retrieveIntervalReads;
    }

    /**
     * D�finit la valeur de la propri�t� retrieveIntervalReads.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetrieveIntervalReads(Boolean value) {
        this.retrieveIntervalReads = value;
    }

    /**
     * Obtient la valeur de la propri�t� demandReset.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDemandReset() {
        return demandReset;
    }

    /**
     * D�finit la valeur de la propri�t� demandReset.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDemandReset(Boolean value) {
        this.demandReset = value;
    }

    /**
     * Obtient la valeur de la propri�t� retrieveEvents.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetrieveEvents() {
        return retrieveEvents;
    }

    /**
     * D�finit la valeur de la propri�t� retrieveEvents.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetrieveEvents(Boolean value) {
        this.retrieveEvents = value;
    }

    /**
     * Obtient la valeur de la propri�t� requestPriority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestPriority() {
        return requestPriority;
    }

    /**
     * D�finit la valeur de la propri�t� requestPriority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestPriority(String value) {
        this.requestPriority = value;
    }

    /**
     * Obtient la valeur de la propri�t� executeStartTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExecuteStartTime() {
        return executeStartTime;
    }

    /**
     * D�finit la valeur de la propri�t� executeStartTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExecuteStartTime(XMLGregorianCalendar value) {
        this.executeStartTime = value;
    }

    /**
     * Obtient la valeur de la propri�t� executeExpireTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExecuteExpireTime() {
        return executeExpireTime;
    }

    /**
     * D�finit la valeur de la propri�t� executeExpireTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExecuteExpireTime(XMLGregorianCalendar value) {
        this.executeExpireTime = value;
    }

}
