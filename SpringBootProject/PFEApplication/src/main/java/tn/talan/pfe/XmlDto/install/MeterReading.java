//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				Only IntervalBlock element will be output in v1 of this
 * 				service implementation.
 * 				
 * 			
 * 
 * <p>Classe Java pour MeterReading complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="MeterReading">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="reasonForRead" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceDeliveryPoint" type="{http://www.emeter.com/energyip/amiinterface}ServiceDeliveryPoint" minOccurs="0"/>
 *         &lt;element name="Meter" type="{http://www.emeter.com/energyip/amiinterface}MeterAsset" minOccurs="0"/>
 *         &lt;element name="ComFunction" type="{http://www.emeter.com/energyip/amiinterface}ComFunction" minOccurs="0"/>
 *         &lt;element name="CustomerAccount" type="{http://www.emeter.com/energyip/amiinterface}CustomerAccount" minOccurs="0"/>
 *         &lt;element name="ServiceLocation" type="{http://www.emeter.com/energyip/amiinterface}ServiceLocation" minOccurs="0"/>
 *         &lt;element name="Channel" type="{http://www.emeter.com/energyip/amiinterface}Channel" minOccurs="0"/>
 *         &lt;element name="DistributionNode" type="{http://www.emeter.com/energyip/amiinterface}DistributionNode" minOccurs="0"/>
 *         &lt;element name="Reading" type="{http://www.emeter.com/energyip/amiinterface}Reading" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IntervalBlock" type="{http://www.emeter.com/energyip/amiinterface}IntervalBlock" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Event" type="{http://www.emeter.com/energyip/amiinterface}Event" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeterReading", propOrder = {
    "reasonForRead",
    "serviceDeliveryPoint",
    "meter",
    "comFunction",
    "customerAccount",
    "serviceLocation",
    "channel",
    "distributionNode",
    "reading",
    "intervalBlock",
    "event"
})
public class MeterReading {

    protected String reasonForRead;
    @XmlElement(name = "ServiceDeliveryPoint")
    protected ServiceDeliveryPoint serviceDeliveryPoint;
    @XmlElement(name = "Meter")
    protected MeterAsset meter;
    @XmlElement(name = "ComFunction")
    protected ComFunction comFunction;
    @XmlElement(name = "CustomerAccount")
    protected CustomerAccount customerAccount;
    @XmlElement(name = "ServiceLocation")
    protected ServiceLocation serviceLocation;
    @XmlElement(name = "Channel")
    protected Channel channel;
    @XmlElement(name = "DistributionNode")
    protected DistributionNode distributionNode;
    @XmlElement(name = "Reading")
    protected List<Reading> reading;
    @XmlElement(name = "IntervalBlock")
    protected List<IntervalBlock> intervalBlock;
    @XmlElement(name = "Event")
    protected List<Event> event;

    /**
     * Obtient la valeur de la propri�t� reasonForRead.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonForRead() {
        return reasonForRead;
    }

    /**
     * D�finit la valeur de la propri�t� reasonForRead.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonForRead(String value) {
        this.reasonForRead = value;
    }

    /**
     * Obtient la valeur de la propri�t� serviceDeliveryPoint.
     * 
     * @return
     *     possible object is
     *     {@link ServiceDeliveryPoint }
     *     
     */
    public ServiceDeliveryPoint getServiceDeliveryPoint() {
        return serviceDeliveryPoint;
    }

    /**
     * D�finit la valeur de la propri�t� serviceDeliveryPoint.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceDeliveryPoint }
     *     
     */
    public void setServiceDeliveryPoint(ServiceDeliveryPoint value) {
        this.serviceDeliveryPoint = value;
    }

    /**
     * Obtient la valeur de la propri�t� meter.
     * 
     * @return
     *     possible object is
     *     {@link MeterAsset }
     *     
     */
    public MeterAsset getMeter() {
        return meter;
    }

    /**
     * D�finit la valeur de la propri�t� meter.
     * 
     * @param value
     *     allowed object is
     *     {@link MeterAsset }
     *     
     */
    public void setMeter(MeterAsset value) {
        this.meter = value;
    }

    /**
     * Obtient la valeur de la propri�t� comFunction.
     * 
     * @return
     *     possible object is
     *     {@link ComFunction }
     *     
     */
    public ComFunction getComFunction() {
        return comFunction;
    }

    /**
     * D�finit la valeur de la propri�t� comFunction.
     * 
     * @param value
     *     allowed object is
     *     {@link ComFunction }
     *     
     */
    public void setComFunction(ComFunction value) {
        this.comFunction = value;
    }

    /**
     * Obtient la valeur de la propri�t� customerAccount.
     * 
     * @return
     *     possible object is
     *     {@link CustomerAccount }
     *     
     */
    public CustomerAccount getCustomerAccount() {
        return customerAccount;
    }

    /**
     * D�finit la valeur de la propri�t� customerAccount.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerAccount }
     *     
     */
    public void setCustomerAccount(CustomerAccount value) {
        this.customerAccount = value;
    }

    /**
     * Obtient la valeur de la propri�t� serviceLocation.
     * 
     * @return
     *     possible object is
     *     {@link ServiceLocation }
     *     
     */
    public ServiceLocation getServiceLocation() {
        return serviceLocation;
    }

    /**
     * D�finit la valeur de la propri�t� serviceLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceLocation }
     *     
     */
    public void setServiceLocation(ServiceLocation value) {
        this.serviceLocation = value;
    }

    /**
     * Obtient la valeur de la propri�t� channel.
     * 
     * @return
     *     possible object is
     *     {@link Channel }
     *     
     */
    public Channel getChannel() {
        return channel;
    }

    /**
     * D�finit la valeur de la propri�t� channel.
     * 
     * @param value
     *     allowed object is
     *     {@link Channel }
     *     
     */
    public void setChannel(Channel value) {
        this.channel = value;
    }

    /**
     * Obtient la valeur de la propri�t� distributionNode.
     * 
     * @return
     *     possible object is
     *     {@link DistributionNode }
     *     
     */
    public DistributionNode getDistributionNode() {
        return distributionNode;
    }

    /**
     * D�finit la valeur de la propri�t� distributionNode.
     * 
     * @param value
     *     allowed object is
     *     {@link DistributionNode }
     *     
     */
    public void setDistributionNode(DistributionNode value) {
        this.distributionNode = value;
    }

    /**
     * Gets the value of the reading property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reading property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReading().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Reading }
     * 
     * 
     */
    public List<Reading> getReading() {
        if (reading == null) {
            reading = new ArrayList<Reading>();
        }
        return this.reading;
    }

    /**
     * Gets the value of the intervalBlock property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the intervalBlock property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntervalBlock().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntervalBlock }
     * 
     * 
     */
    public List<IntervalBlock> getIntervalBlock() {
        if (intervalBlock == null) {
            intervalBlock = new ArrayList<IntervalBlock>();
        }
        return this.intervalBlock;
    }

    /**
     * Gets the value of the event property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the event property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEvent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Event }
     * 
     * 
     */
    public List<Event> getEvent() {
        if (event == null) {
            event = new ArrayList<Event>();
        }
        return this.event;
    }

}
