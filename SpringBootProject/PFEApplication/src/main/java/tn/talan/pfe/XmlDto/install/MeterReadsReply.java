//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour MeterReadsReply complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="MeterReadsReply">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.emeter.com/energyip/amiinterface}MeterReadsMessageType">
 *       &lt;sequence>
 *         &lt;element name="reply" type="{http://www.emeter.com/energyip/amiinterface}ReplyHeader" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeterReadsReply", propOrder = {
    "reply"
})
public class MeterReadsReply
    extends MeterReadsMessageType
{

    protected ReplyHeader reply;

    /**
     * Obtient la valeur de la propri�t� reply.
     * 
     * @return
     *     possible object is
     *     {@link ReplyHeader }
     *     
     */
    public ReplyHeader getReply() {
        return reply;
    }

    /**
     * D�finit la valeur de la propri�t� reply.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplyHeader }
     *     
     */
    public void setReply(ReplyHeader value) {
        this.reply = value;
    }

}
