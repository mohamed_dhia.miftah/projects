//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.emeter.energyip.amiinterface package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MeterDataMessage_QNAME = new QName("http://www.emeter.com/energyip/amiinterface", "MeterDataMessage");
    private final static QName _Header_QNAME = new QName("http://www.emeter.com/energyip/amiinterface", "header");
    private final static QName _MeterReading_QNAME = new QName("http://www.emeter.com/energyip/amiinterface", "MeterReading");
    private final static QName _MeterReadsReplyMessage_QNAME = new QName("http://www.emeter.com/energyip/amiinterface", "MeterReadsReplyMessage");
    private final static QName _EventMeasuredValue_QNAME = new QName("http://www.emeter.com/energyip/amiinterface", "measuredValue");
    private final static QName _EventThreshold_QNAME = new QName("http://www.emeter.com/energyip/amiinterface", "threshold");
    private final static QName _EventDuration_QNAME = new QName("http://www.emeter.com/energyip/amiinterface", "duration");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.emeter.energyip.amiinterface
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Event }
     * 
     */
    public Event createEvent() {
        return new Event();
    }

    /**
     * Create an instance of {@link MeterAssetSetLoadLimitRequestMessage }
     * 
     */
    public MeterAssetSetLoadLimitRequestMessage createMeterAssetSetLoadLimitRequestMessage() {
        return new MeterAssetSetLoadLimitRequestMessage();
    }

    /**
     * Create an instance of {@link MessageHeader }
     * 
     */
    public MessageHeader createMessageHeader() {
        return new MessageHeader();
    }

    /**
     * Create an instance of {@link MeterSetLoadLimitRequestPayload }
     * 
     */
    public MeterSetLoadLimitRequestPayload createMeterSetLoadLimitRequestPayload() {
        return new MeterSetLoadLimitRequestPayload();
    }

    /**
     * Create an instance of {@link MeterAssetConnectArmedRequestMessage }
     * 
     */
    public MeterAssetConnectArmedRequestMessage createMeterAssetConnectArmedRequestMessage() {
        return new MeterAssetConnectArmedRequestMessage();
    }

    /**
     * Create an instance of {@link MeterConnectArmedRequestPayload }
     * 
     */
    public MeterConnectArmedRequestPayload createMeterConnectArmedRequestPayload() {
        return new MeterConnectArmedRequestPayload();
    }

    /**
     * Create an instance of {@link MeterAssetCheckLoadLimitReplyMessage }
     * 
     */
    public MeterAssetCheckLoadLimitReplyMessage createMeterAssetCheckLoadLimitReplyMessage() {
        return new MeterAssetCheckLoadLimitReplyMessage();
    }

    /**
     * Create an instance of {@link CheckLoadLimitReplyHeader }
     * 
     */
    public CheckLoadLimitReplyHeader createCheckLoadLimitReplyHeader() {
        return new CheckLoadLimitReplyHeader();
    }

    /**
     * Create an instance of {@link MeterAssetAddReplyMessage }
     * 
     */
    public MeterAssetAddReplyMessage createMeterAssetAddReplyMessage() {
        return new MeterAssetAddReplyMessage();
    }

    /**
     * Create an instance of {@link ReplyHeader }
     * 
     */
    public ReplyHeader createReplyHeader() {
        return new ReplyHeader();
    }

    /**
     * Create an instance of {@link MeterAssetCheckMeterConfigurationReplyMessage }
     * 
     */
    public MeterAssetCheckMeterConfigurationReplyMessage createMeterAssetCheckMeterConfigurationReplyMessage() {
        return new MeterAssetCheckMeterConfigurationReplyMessage();
    }

    /**
     * Create an instance of {@link CheckMeterConfigurationReplyHeader }
     * 
     */
    public CheckMeterConfigurationReplyHeader createCheckMeterConfigurationReplyHeader() {
        return new CheckMeterConfigurationReplyHeader();
    }

    /**
     * Create an instance of {@link MeterAssetUpdateReplyMessage }
     * 
     */
    public MeterAssetUpdateReplyMessage createMeterAssetUpdateReplyMessage() {
        return new MeterAssetUpdateReplyMessage();
    }

    /**
     * Create an instance of {@link MeterReadings }
     * 
     */
    public MeterReadings createMeterReadings() {
        return new MeterReadings();
    }

    /**
     * Create an instance of {@link MeterReading }
     * 
     */
    public MeterReading createMeterReading() {
        return new MeterReading();
    }

    /**
     * Create an instance of {@link MeterAssetUpdateRequestMessage }
     * 
     */
    public MeterAssetUpdateRequestMessage createMeterAssetUpdateRequestMessage() {
        return new MeterAssetUpdateRequestMessage();
    }

    /**
     * Create an instance of {@link MeterUpdateRequestPayload }
     * 
     */
    public MeterUpdateRequestPayload createMeterUpdateRequestPayload() {
        return new MeterUpdateRequestPayload();
    }

    /**
     * Create an instance of {@link MeterAssetCheckLoadLimitRequestMessage }
     * 
     */
    public MeterAssetCheckLoadLimitRequestMessage createMeterAssetCheckLoadLimitRequestMessage() {
        return new MeterAssetCheckLoadLimitRequestMessage();
    }

    /**
     * Create an instance of {@link MeterCheckLoadLimitRequestPayload }
     * 
     */
    public MeterCheckLoadLimitRequestPayload createMeterCheckLoadLimitRequestPayload() {
        return new MeterCheckLoadLimitRequestPayload();
    }

    /**
     * Create an instance of {@link MeterAssetCheckMeterConfigurationRequestMessage }
     * 
     */
    public MeterAssetCheckMeterConfigurationRequestMessage createMeterAssetCheckMeterConfigurationRequestMessage() {
        return new MeterAssetCheckMeterConfigurationRequestMessage();
    }

    /**
     * Create an instance of {@link CheckMeterConfigurationRequestPayload }
     * 
     */
    public CheckMeterConfigurationRequestPayload createCheckMeterConfigurationRequestPayload() {
        return new CheckMeterConfigurationRequestPayload();
    }

    /**
     * Create an instance of {@link MeterAssetConnectRequestMessage }
     * 
     */
    public MeterAssetConnectRequestMessage createMeterAssetConnectRequestMessage() {
        return new MeterAssetConnectRequestMessage();
    }

    /**
     * Create an instance of {@link MeterConnectRequestPayload }
     * 
     */
    public MeterConnectRequestPayload createMeterConnectRequestPayload() {
        return new MeterConnectRequestPayload();
    }

    /**
     * Create an instance of {@link MeterAssetPingReplyMessage }
     * 
     */
    public MeterAssetPingReplyMessage createMeterAssetPingReplyMessage() {
        return new MeterAssetPingReplyMessage();
    }

    /**
     * Create an instance of {@link ExtReplyHeader }
     * 
     */
    public ExtReplyHeader createExtReplyHeader() {
        return new ExtReplyHeader();
    }

    /**
     * Create an instance of {@link MeterAssetConnectReplyMessage }
     * 
     */
    public MeterAssetConnectReplyMessage createMeterAssetConnectReplyMessage() {
        return new MeterAssetConnectReplyMessage();
    }

    /**
     * Create an instance of {@link RequestMessage }
     * 
     */
    public RequestMessage createRequestMessage() {
        return new RequestMessage();
    }

    /**
     * Create an instance of {@link MeterAssetDeleteRequestMessage }
     * 
     */
    public MeterAssetDeleteRequestMessage createMeterAssetDeleteRequestMessage() {
        return new MeterAssetDeleteRequestMessage();
    }

    /**
     * Create an instance of {@link MeterDeleteRequestPayload }
     * 
     */
    public MeterDeleteRequestPayload createMeterDeleteRequestPayload() {
        return new MeterDeleteRequestPayload();
    }

    /**
     * Create an instance of {@link JobCancelRequestMessage }
     * 
     */
    public JobCancelRequestMessage createJobCancelRequestMessage() {
        return new JobCancelRequestMessage();
    }

    /**
     * Create an instance of {@link JobCancelRequest }
     * 
     */
    public JobCancelRequest createJobCancelRequest() {
        return new JobCancelRequest();
    }

    /**
     * Create an instance of {@link MeterReadsMessageType }
     * 
     */
    public MeterReadsMessageType createMeterReadsMessageType() {
        return new MeterReadsMessageType();
    }

    /**
     * Create an instance of {@link MeterAssetRetireReplyMessage }
     * 
     */
    public MeterAssetRetireReplyMessage createMeterAssetRetireReplyMessage() {
        return new MeterAssetRetireReplyMessage();
    }

    /**
     * Create an instance of {@link MeterAssetConnectArmedReplyMessage }
     * 
     */
    public MeterAssetConnectArmedReplyMessage createMeterAssetConnectArmedReplyMessage() {
        return new MeterAssetConnectArmedReplyMessage();
    }

    /**
     * Create an instance of {@link MeterAssetDeleteReplyMessage }
     * 
     */
    public MeterAssetDeleteReplyMessage createMeterAssetDeleteReplyMessage() {
        return new MeterAssetDeleteReplyMessage();
    }

    /**
     * Create an instance of {@link MeterAssetDisconnectRequestMessage }
     * 
     */
    public MeterAssetDisconnectRequestMessage createMeterAssetDisconnectRequestMessage() {
        return new MeterAssetDisconnectRequestMessage();
    }

    /**
     * Create an instance of {@link MeterDisconnectRequestPayload }
     * 
     */
    public MeterDisconnectRequestPayload createMeterDisconnectRequestPayload() {
        return new MeterDisconnectRequestPayload();
    }

    /**
     * Create an instance of {@link MeterAssetSendTextRequestMessage }
     * 
     */
    public MeterAssetSendTextRequestMessage createMeterAssetSendTextRequestMessage() {
        return new MeterAssetSendTextRequestMessage();
    }

    /**
     * Create an instance of {@link MeterSendTextMessageRequestPayload }
     * 
     */
    public MeterSendTextMessageRequestPayload createMeterSendTextMessageRequestPayload() {
        return new MeterSendTextMessageRequestPayload();
    }

    /**
     * Create an instance of {@link MeterReadsRequestMessage }
     * 
     */
    public MeterReadsRequestMessage createMeterReadsRequestMessage() {
        return new MeterReadsRequestMessage();
    }

    /**
     * Create an instance of {@link MeterDataRequest }
     * 
     */
    public MeterDataRequest createMeterDataRequest() {
        return new MeterDataRequest();
    }

    /**
     * Create an instance of {@link MeterAssetSendTextReplyMessage }
     * 
     */
    public MeterAssetSendTextReplyMessage createMeterAssetSendTextReplyMessage() {
        return new MeterAssetSendTextReplyMessage();
    }

    /**
     * Create an instance of {@link MeterAssetCDCStatusReplyMessage }
     * 
     */
    public MeterAssetCDCStatusReplyMessage createMeterAssetCDCStatusReplyMessage() {
        return new MeterAssetCDCStatusReplyMessage();
    }

    /**
     * Create an instance of {@link ReplyMessage }
     * 
     */
    public ReplyMessage createReplyMessage() {
        return new ReplyMessage();
    }

    /**
     * Create an instance of {@link MeterReadsReply }
     * 
     */
    public MeterReadsReply createMeterReadsReply() {
        return new MeterReadsReply();
    }

    /**
     * Create an instance of {@link MeterAssetRetireRequestMessage }
     * 
     */
    public MeterAssetRetireRequestMessage createMeterAssetRetireRequestMessage() {
        return new MeterAssetRetireRequestMessage();
    }

    /**
     * Create an instance of {@link MeterRetireRequestPayload }
     * 
     */
    public MeterRetireRequestPayload createMeterRetireRequestPayload() {
        return new MeterRetireRequestPayload();
    }

    /**
     * Create an instance of {@link MeterAssetSetLoadLimitReplyMessage }
     * 
     */
    public MeterAssetSetLoadLimitReplyMessage createMeterAssetSetLoadLimitReplyMessage() {
        return new MeterAssetSetLoadLimitReplyMessage();
    }

    /**
     * Create an instance of {@link SetLoadLimitReplyHeader }
     * 
     */
    public SetLoadLimitReplyHeader createSetLoadLimitReplyHeader() {
        return new SetLoadLimitReplyHeader();
    }

    /**
     * Create an instance of {@link MeterAssetAddRequestMessage }
     * 
     */
    public MeterAssetAddRequestMessage createMeterAssetAddRequestMessage() {
        return new MeterAssetAddRequestMessage();
    }

    /**
     * Create an instance of {@link MeterAddRequestPayload }
     * 
     */
    public MeterAddRequestPayload createMeterAddRequestPayload() {
        return new MeterAddRequestPayload();
    }

    /**
     * Create an instance of {@link MeterAssetPingRequestMessage }
     * 
     */
    public MeterAssetPingRequestMessage createMeterAssetPingRequestMessage() {
        return new MeterAssetPingRequestMessage();
    }

    /**
     * Create an instance of {@link PingRequest }
     * 
     */
    public PingRequest createPingRequest() {
        return new PingRequest();
    }

    /**
     * Create an instance of {@link JobCancelReplyMessage }
     * 
     */
    public JobCancelReplyMessage createJobCancelReplyMessage() {
        return new JobCancelReplyMessage();
    }

    /**
     * Create an instance of {@link MeterAssetCDCStatusRequestMessage }
     * 
     */
    public MeterAssetCDCStatusRequestMessage createMeterAssetCDCStatusRequestMessage() {
        return new MeterAssetCDCStatusRequestMessage();
    }

    /**
     * Create an instance of {@link MeterCDCStatusRequestPayload }
     * 
     */
    public MeterCDCStatusRequestPayload createMeterCDCStatusRequestPayload() {
        return new MeterCDCStatusRequestPayload();
    }

    /**
     * Create an instance of {@link MeterAssetDisconnectReplyMessage }
     * 
     */
    public MeterAssetDisconnectReplyMessage createMeterAssetDisconnectReplyMessage() {
        return new MeterAssetDisconnectReplyMessage();
    }

    /**
     * Create an instance of {@link GasMeter }
     * 
     */
    public GasMeter createGasMeter() {
        return new GasMeter();
    }

    /**
     * Create an instance of {@link MeterChangeRequestPayload }
     * 
     */
    public MeterChangeRequestPayload createMeterChangeRequestPayload() {
        return new MeterChangeRequestPayload();
    }

    /**
     * Create an instance of {@link CollectionGroup }
     * 
     */
    public CollectionGroup createCollectionGroup() {
        return new CollectionGroup();
    }

    /**
     * Create an instance of {@link Asset }
     * 
     */
    public Asset createAsset() {
        return new Asset();
    }

    /**
     * Create an instance of {@link ComFunction }
     * 
     */
    public ComFunction createComFunction() {
        return new ComFunction();
    }

    /**
     * Create an instance of {@link Channel }
     * 
     */
    public Channel createChannel() {
        return new Channel();
    }

    /**
     * Create an instance of {@link ErpAddress }
     * 
     */
    public ErpAddress createErpAddress() {
        return new ErpAddress();
    }

    /**
     * Create an instance of {@link CustomerAccount }
     * 
     */
    public CustomerAccount createCustomerAccount() {
        return new CustomerAccount();
    }

    /**
     * Create an instance of {@link ElectricMeter }
     * 
     */
    public ElectricMeter createElectricMeter() {
        return new ElectricMeter();
    }

    /**
     * Create an instance of {@link ReadingTypeList }
     * 
     */
    public ReadingTypeList createReadingTypeList() {
        return new ReadingTypeList();
    }

    /**
     * Create an instance of {@link IdentifiedObject }
     * 
     */
    public IdentifiedObject createIdentifiedObject() {
        return new IdentifiedObject();
    }

    /**
     * Create an instance of {@link MeterAsset }
     * 
     */
    public MeterAsset createMeterAsset() {
        return new MeterAsset();
    }

    /**
     * Create an instance of {@link IntervalBlock }
     * 
     */
    public IntervalBlock createIntervalBlock() {
        return new IntervalBlock();
    }

    /**
     * Create an instance of {@link ReadingType }
     * 
     */
    public ReadingType createReadingType() {
        return new ReadingType();
    }

    /**
     * Create an instance of {@link ConfigurationProfileStatus }
     * 
     */
    public ConfigurationProfileStatus createConfigurationProfileStatus() {
        return new ConfigurationProfileStatus();
    }

    /**
     * Create an instance of {@link ServiceLocation }
     * 
     */
    public ServiceLocation createServiceLocation() {
        return new ServiceLocation();
    }

    /**
     * Create an instance of {@link Quality }
     * 
     */
    public Quality createQuality() {
        return new Quality();
    }

    /**
     * Create an instance of {@link IReading }
     * 
     */
    public IReading createIReading() {
        return new IReading();
    }

    /**
     * Create an instance of {@link DistributionNode }
     * 
     */
    public DistributionNode createDistributionNode() {
        return new DistributionNode();
    }

    /**
     * Create an instance of {@link ProvisioningGroup }
     * 
     */
    public ProvisioningGroup createProvisioningGroup() {
        return new ProvisioningGroup();
    }

    /**
     * Create an instance of {@link ConfigurationProfile }
     * 
     */
    public ConfigurationProfile createConfigurationProfile() {
        return new ConfigurationProfile();
    }

    /**
     * Create an instance of {@link WaterMeter }
     * 
     */
    public WaterMeter createWaterMeter() {
        return new WaterMeter();
    }

    /**
     * Create an instance of {@link Reading }
     * 
     */
    public Reading createReading() {
        return new Reading();
    }

    /**
     * Create an instance of {@link BroadcastGroup }
     * 
     */
    public BroadcastGroup createBroadcastGroup() {
        return new BroadcastGroup();
    }

    /**
     * Create an instance of {@link ServiceDeliveryPoint }
     * 
     */
    public ServiceDeliveryPoint createServiceDeliveryPoint() {
        return new ServiceDeliveryPoint();
    }

    /**
     * Create an instance of {@link Parameter }
     * 
     */
    public Parameter createParameter() {
        return new Parameter();
    }

    /**
     * Create an instance of {@link Event.Argument }
     * 
     */
    public Event.Argument createEventArgument() {
        return new Event.Argument();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MeterReadsMessageType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.emeter.com/energyip/amiinterface", name = "MeterDataMessage")
    public JAXBElement<MeterReadsMessageType> createMeterDataMessage(MeterReadsMessageType value) {
        return new JAXBElement<MeterReadsMessageType>(_MeterDataMessage_QNAME, MeterReadsMessageType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageHeader }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.emeter.com/energyip/amiinterface", name = "header")
    public JAXBElement<MessageHeader> createHeader(MessageHeader value) {
        return new JAXBElement<MessageHeader>(_Header_QNAME, MessageHeader.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MeterReading }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.emeter.com/energyip/amiinterface", name = "MeterReading")
    public JAXBElement<MeterReading> createMeterReading(MeterReading value) {
        return new JAXBElement<MeterReading>(_MeterReading_QNAME, MeterReading.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MeterReadsReply }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.emeter.com/energyip/amiinterface", name = "MeterReadsReplyMessage")
    public JAXBElement<MeterReadsReply> createMeterReadsReplyMessage(MeterReadsReply value) {
        return new JAXBElement<MeterReadsReply>(_MeterReadsReplyMessage_QNAME, MeterReadsReply.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.emeter.com/energyip/amiinterface", name = "measuredValue", scope = Event.class)
    public JAXBElement<Double> createEventMeasuredValue(Double value) {
        return new JAXBElement<Double>(_EventMeasuredValue_QNAME, Double.class, Event.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.emeter.com/energyip/amiinterface", name = "threshold", scope = Event.class)
    public JAXBElement<Double> createEventThreshold(Double value) {
        return new JAXBElement<Double>(_EventThreshold_QNAME, Double.class, Event.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.emeter.com/energyip/amiinterface", name = "duration", scope = Event.class)
    public JAXBElement<Double> createEventDuration(Double value) {
        return new JAXBElement<Double>(_EventDuration_QNAME, Double.class, Event.class, value);
    }

}
