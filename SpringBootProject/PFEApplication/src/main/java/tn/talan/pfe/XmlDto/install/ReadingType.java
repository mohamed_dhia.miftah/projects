//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ReadingType complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ReadingType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="mRID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="measurementType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="touBinNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="unit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="touCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="channelNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="logicalChannelNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="aggregateTimePeriod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="intervalLength" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="readSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="precision" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="scale" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="direction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReadingType", propOrder = {
    "mrid",
    "measurementType",
    "touBinNumber",
    "unit",
    "touCode",
    "channelNumber",
    "logicalChannelNumber",
    "aggregateTimePeriod",
    "intervalLength",
    "readSequence",
    "precision",
    "scale",
    "direction"
})
public class ReadingType {

    @XmlElement(name = "mRID")
    protected String mrid;
    protected String measurementType;
    protected String touBinNumber;
    protected String unit;
    protected String touCode;
    protected Integer channelNumber;
    protected Integer logicalChannelNumber;
    protected String aggregateTimePeriod;
    protected Integer intervalLength;
    protected String readSequence;
    protected Integer precision;
    protected Integer scale;
    protected String direction;

    /**
     * Obtient la valeur de la propri�t� mrid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMRID() {
        return mrid;
    }

    /**
     * D�finit la valeur de la propri�t� mrid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMRID(String value) {
        this.mrid = value;
    }

    /**
     * Obtient la valeur de la propri�t� measurementType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasurementType() {
        return measurementType;
    }

    /**
     * D�finit la valeur de la propri�t� measurementType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasurementType(String value) {
        this.measurementType = value;
    }

    /**
     * Obtient la valeur de la propri�t� touBinNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTouBinNumber() {
        return touBinNumber;
    }

    /**
     * D�finit la valeur de la propri�t� touBinNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTouBinNumber(String value) {
        this.touBinNumber = value;
    }

    /**
     * Obtient la valeur de la propri�t� unit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnit() {
        return unit;
    }

    /**
     * D�finit la valeur de la propri�t� unit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnit(String value) {
        this.unit = value;
    }

    /**
     * Obtient la valeur de la propri�t� touCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTouCode() {
        return touCode;
    }

    /**
     * D�finit la valeur de la propri�t� touCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTouCode(String value) {
        this.touCode = value;
    }

    /**
     * Obtient la valeur de la propri�t� channelNumber.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChannelNumber() {
        return channelNumber;
    }

    /**
     * D�finit la valeur de la propri�t� channelNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChannelNumber(Integer value) {
        this.channelNumber = value;
    }

    /**
     * Obtient la valeur de la propri�t� logicalChannelNumber.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLogicalChannelNumber() {
        return logicalChannelNumber;
    }

    /**
     * D�finit la valeur de la propri�t� logicalChannelNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLogicalChannelNumber(Integer value) {
        this.logicalChannelNumber = value;
    }

    /**
     * Obtient la valeur de la propri�t� aggregateTimePeriod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAggregateTimePeriod() {
        return aggregateTimePeriod;
    }

    /**
     * D�finit la valeur de la propri�t� aggregateTimePeriod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAggregateTimePeriod(String value) {
        this.aggregateTimePeriod = value;
    }

    /**
     * Obtient la valeur de la propri�t� intervalLength.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIntervalLength() {
        return intervalLength;
    }

    /**
     * D�finit la valeur de la propri�t� intervalLength.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIntervalLength(Integer value) {
        this.intervalLength = value;
    }

    /**
     * Obtient la valeur de la propri�t� readSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReadSequence() {
        return readSequence;
    }

    /**
     * D�finit la valeur de la propri�t� readSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReadSequence(String value) {
        this.readSequence = value;
    }

    /**
     * Obtient la valeur de la propri�t� precision.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPrecision() {
        return precision;
    }

    /**
     * D�finit la valeur de la propri�t� precision.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPrecision(Integer value) {
        this.precision = value;
    }

    /**
     * Obtient la valeur de la propri�t� scale.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getScale() {
        return scale;
    }

    /**
     * D�finit la valeur de la propri�t� scale.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setScale(Integer value) {
        this.scale = value;
    }

    /**
     * Obtient la valeur de la propri�t� direction.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirection() {
        return direction;
    }

    /**
     * D�finit la valeur de la propri�t� direction.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirection(String value) {
        this.direction = value;
    }

}
