//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ReplyHeader complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ReplyHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="replyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="replyText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="correlationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReplyHeader", propOrder = {
    "replyCode",
    "replyText",
    "correlationId"
})
@XmlSeeAlso({
    CheckLoadLimitReplyHeader.class,
    CheckMeterConfigurationReplyHeader.class,
    ExtReplyHeader.class,
    SetLoadLimitReplyHeader.class
})
public class ReplyHeader {

    @XmlElement(required = true)
    protected String replyCode;
    protected String replyText;
    protected String correlationId;

    /**
     * Obtient la valeur de la propri�t� replyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReplyCode() {
        return replyCode;
    }

    /**
     * D�finit la valeur de la propri�t� replyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReplyCode(String value) {
        this.replyCode = value;
    }

    /**
     * Obtient la valeur de la propri�t� replyText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReplyText() {
        return replyText;
    }

    /**
     * D�finit la valeur de la propri�t� replyText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReplyText(String value) {
        this.replyText = value;
    }

    /**
     * Obtient la valeur de la propri�t� correlationId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationId() {
        return correlationId;
    }

    /**
     * D�finit la valeur de la propri�t� correlationId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationId(String value) {
        this.correlationId = value;
    }

}
