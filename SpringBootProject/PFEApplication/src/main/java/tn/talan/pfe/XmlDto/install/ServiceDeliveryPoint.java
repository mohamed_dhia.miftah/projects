//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ServiceDeliveryPoint complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ServiceDeliveryPoint">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.emeter.com/energyip/amiinterface}IdentifiedObject">
 *       &lt;sequence>
 *         &lt;element name="serviceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lifeSupport" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="readRoute" type="{http://www.emeter.com/energyip/amiinterface}IdentifiedObject" minOccurs="0"/>
 *         &lt;element name="readSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="averageDailyUsage" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="locationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="serviceDeliveryRemarks" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="latitude" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="longitude" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="locationAccuracy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locationAccuracyUnits" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceDeliveryPoint", propOrder = {
    "serviceType",
    "lifeSupport",
    "readRoute",
    "readSequence",
    "averageDailyUsage",
    "locationCode",
    "serviceDeliveryRemarks",
    "latitude",
    "longitude",
    "locationAccuracy",
    "locationAccuracyUnits"
})
public class ServiceDeliveryPoint
    extends IdentifiedObject
{

    protected String serviceType;
    protected Boolean lifeSupport;
    protected IdentifiedObject readRoute;
    protected String readSequence;
    protected Double averageDailyUsage;
    @XmlElement(required = true)
    protected String locationCode;
    @XmlElement(required = true)
    protected String serviceDeliveryRemarks;
    protected double latitude;
    protected double longitude;
    protected String locationAccuracy;
    protected String locationAccuracyUnits;

    /**
     * Obtient la valeur de la propri�t� serviceType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * D�finit la valeur de la propri�t� serviceType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceType(String value) {
        this.serviceType = value;
    }

    /**
     * Obtient la valeur de la propri�t� lifeSupport.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLifeSupport() {
        return lifeSupport;
    }

    /**
     * D�finit la valeur de la propri�t� lifeSupport.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLifeSupport(Boolean value) {
        this.lifeSupport = value;
    }

    /**
     * Obtient la valeur de la propri�t� readRoute.
     * 
     * @return
     *     possible object is
     *     {@link IdentifiedObject }
     *     
     */
    public IdentifiedObject getReadRoute() {
        return readRoute;
    }

    /**
     * D�finit la valeur de la propri�t� readRoute.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifiedObject }
     *     
     */
    public void setReadRoute(IdentifiedObject value) {
        this.readRoute = value;
    }

    /**
     * Obtient la valeur de la propri�t� readSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReadSequence() {
        return readSequence;
    }

    /**
     * D�finit la valeur de la propri�t� readSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReadSequence(String value) {
        this.readSequence = value;
    }

    /**
     * Obtient la valeur de la propri�t� averageDailyUsage.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAverageDailyUsage() {
        return averageDailyUsage;
    }

    /**
     * D�finit la valeur de la propri�t� averageDailyUsage.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAverageDailyUsage(Double value) {
        this.averageDailyUsage = value;
    }

    /**
     * Obtient la valeur de la propri�t� locationCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationCode() {
        return locationCode;
    }

    /**
     * D�finit la valeur de la propri�t� locationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationCode(String value) {
        this.locationCode = value;
    }

    /**
     * Obtient la valeur de la propri�t� serviceDeliveryRemarks.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceDeliveryRemarks() {
        return serviceDeliveryRemarks;
    }

    /**
     * D�finit la valeur de la propri�t� serviceDeliveryRemarks.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceDeliveryRemarks(String value) {
        this.serviceDeliveryRemarks = value;
    }

    /**
     * Obtient la valeur de la propri�t� latitude.
     * 
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * D�finit la valeur de la propri�t� latitude.
     * 
     */
    public void setLatitude(double value) {
        this.latitude = value;
    }

    /**
     * Obtient la valeur de la propri�t� longitude.
     * 
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * D�finit la valeur de la propri�t� longitude.
     * 
     */
    public void setLongitude(double value) {
        this.longitude = value;
    }

    /**
     * Obtient la valeur de la propri�t� locationAccuracy.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationAccuracy() {
        return locationAccuracy;
    }

    /**
     * D�finit la valeur de la propri�t� locationAccuracy.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationAccuracy(String value) {
        this.locationAccuracy = value;
    }

    /**
     * Obtient la valeur de la propri�t� locationAccuracyUnits.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationAccuracyUnits() {
        return locationAccuracyUnits;
    }

    /**
     * D�finit la valeur de la propri�t� locationAccuracyUnits.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationAccuracyUnits(String value) {
        this.locationAccuracyUnits = value;
    }

}
