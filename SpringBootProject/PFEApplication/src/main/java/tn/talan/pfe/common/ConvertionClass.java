package tn.talan.pfe.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.datatype.XMLGregorianCalendar;

public class ConvertionClass {

	public Date convertStringToDate(String stringDate) throws ParseException {
		String dateString = stringDate.substring(6, 7) + "/" + stringDate.substring(4, 6) + "/"
				+ stringDate.substring(0, 4);
		return new SimpleDateFormat("dd/MM/yyyy").parse(dateString);
	}

	public int convertStringToInt(String stringInt) {
		return Integer.parseInt(stringInt);
	}

	public Date convertxmlGregorianCalendartoDate(XMLGregorianCalendar xmlDate) {
		return xmlDate.toGregorianCalendar().getTime();
	}
}
