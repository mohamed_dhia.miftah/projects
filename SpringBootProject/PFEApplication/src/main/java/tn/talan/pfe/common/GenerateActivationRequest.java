package tn.talan.pfe.common;

import java.io.StringWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.Version;

public class GenerateActivationRequest {

	private static Configuration cfg = new Configuration(new Version("2.3.0"));

	public String generate(String serialNumber) {
		try {
			// Load template
			Template template = cfg.getTemplate("src/main/resources/ActivationRequest/ActivationRequest.ftl");

			// Create data for template
			Map<String, Object> templateData = new HashMap<String, Object>();

			/* get current time + conveting */
			Date date = new Date();
			Timestamp ts = new Timestamp(date.getTime());
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(ts);
			XMLGregorianCalendar Timestamp = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

//			String serialNumber = "serialNumber";
			String correlationID = "correlationID";
			templateData.put("timestamp", Timestamp);
			templateData.put("serialNumber", serialNumber);
			templateData.put("correlationID", correlationID);

			// ********
			StringWriter out = new StringWriter();
			template.process(templateData, out);
			out.flush();

			String activationRequest = out.getBuffer().toString();

			return activationRequest;

		} catch (Exception e) {
			e.printStackTrace();
		}
//		System.out.println("erreur while creating Activation Request");
		return "erreur while creating Activation Request";
	}
}
