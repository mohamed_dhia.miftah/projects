package tn.talan.pfe.common;

import java.io.File;
import java.io.IOException;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;

//import java.io.File;
//import javax.xml.XMLConstants;
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.transform.Result;
//import javax.xml.transform.Source;
//import javax.xml.transform.dom.DOMSource;
//import javax.xml.transform.stream.StreamSource;
//import javax.xml.validation.Schema;
//import javax.xml.validation.SchemaFactory;
//import javax.xml.validation.Validator;
//import org.w3c.dom.Document;

import org.xml.sax.SAXException;

public class XMLValidation {

	public boolean validation(String xmlFileName, String xsdFileName) {

		boolean flag = true;
//		 String xsdFileName = "xsdResources\\Fluvius_SF_gas_v1.5.xsd";
//		 String xmlFileName = "uploadDirectory\\AMMSAG00001234567-00010-251234100-0.20130405185147_Meter_amm.xml";
		try {
			validate(xmlFileName, xsdFileName);
		} catch (SAXException e) {
			flag = false;
			e.printStackTrace();
		} catch (IOException e) {
			flag = false;
			e.printStackTrace();
		}
		System.out.println("xml file is valid : " + flag);
		return flag;
	}

	public void validate(String xmlFile, String validationFile) throws SAXException, IOException {
		SchemaFactory shemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		((shemaFactory.newSchema(new File(validationFile))).newValidator())
				.validate(new StreamSource(new File(xmlFile)));
	}

	// 2eme methode
	public void validateXml() {

//		// parse an XML document into a DOM tree
//		DocumentBuilder parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
//		Document document = parser.parse(new File(file.getOriginalFilename()));
//
//		// create a SchemaFactory capable of understanding WXS schemas
//		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
//
//		// load a WXS schema, represented by a Schema instance
//		Source schemaFile = new StreamSource(new File("Fluvius_SF_gas_v1.5.xsd"));
//		Schema schema = factory.newSchema(schemaFile);
//
//		// create a Validator instance, which can be used to validate an instance
//		// document
//		Validator validator = schema.newValidator();
//
//		// validate the DOM tree
//		Result Result = null;
//		validator.validate(new DOMSource(document), Result);
//
//		System.out.println(Result);

	}
}
