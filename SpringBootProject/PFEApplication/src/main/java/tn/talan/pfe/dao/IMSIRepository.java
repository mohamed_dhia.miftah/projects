package tn.talan.pfe.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import tn.talan.pfe.entities.IMSI;

public interface IMSIRepository extends JpaRepository<IMSI, String> {
}
