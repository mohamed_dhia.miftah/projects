package tn.talan.pfe.entities;

import java.io.Serializable;

import javax.xml.datatype.XMLGregorianCalendar;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActivationRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private XMLGregorianCalendar timestamp;
	private String correlationID;
	private String serialNumber;
}
