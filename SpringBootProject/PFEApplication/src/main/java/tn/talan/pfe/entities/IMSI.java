package tn.talan.pfe.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "imsi", schema = "public")
public class IMSI implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "imsi_number", unique = true, nullable = false, length = 15)
	private String imsiNumber;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "meter_serial_number", nullable = false)
	private Meter meter;
}