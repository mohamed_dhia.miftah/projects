package tn.talan.pfe.entities;
// Generated 10 avr. 2019 16:00:40 by Hibernate Tools 5.2.10.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "meter", schema = "public")
public class Meter implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private String serialNumber;

	private String palletId;
	private String boxId;
	private String customerLotNumber;
	private String testReport;
	private String certificationNumberReport;
	private String deviceBatchNumber;
	private String communicationMethod;

	private int customerArticleNumber;

	private short certificationYear;
	private short yearOfManufactory;

	private String hardwareVersion;
	private String moduleActiveFirmwareVersion;
	private String moduleTargetFirmwareVersion;
	private String coreActiveFirmwareVersion;
	private String auxiliaryModuleActiveFirmwareVersion;
	private String lifeCycleStatus;
	private String batteryType;

	@Temporal(TemporalType.DATE)
	private Date startOfBatteryLifeDate;
	@Temporal(TemporalType.DATE)
	private Date batteryReplacementDate;

	private String communicationProvider;
	private String communicationProviderBatchId;
	private String iccId;
	private String imei;
	private String communicationModuleId;
	private String pppAuthenticationUser1;
	private String pppAuthenticationUser2;
	private String pppAuthenticationPassword;
	private String modemHardwareVersion;
	private String modemFirmwareVersion;
	private String equipmentIdentifier;

	private Short hlsActive;

	@Temporal(TemporalType.TIMESTAMP)
	private Date discoveryDateTime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date installDateTime;

	private ShipmentFile shipmentfile;
	private Set<IMSI> imsis = new HashSet<IMSI>(0);
	private Set<Mbus> mbuses = new HashSet<Mbus>(0);

	@Id
	@Column(name = "serial_number", unique = true, nullable = false, length = 14)
	public String getSerialNumber() {
		return this.serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shipment_file_name", nullable = false)
	public ShipmentFile getShipmentfile() {
		return this.shipmentfile;
	}

	public void setShipmentfile(ShipmentFile shipmentfile) {
		this.shipmentfile = shipmentfile;
	}

	@Column(name = "pallet_id", nullable = false, length = 30)
	public String getPalletId() {
		return this.palletId;
	}

	public void setPalletId(String palletId) {
		this.palletId = palletId;
	}

	@Column(name = "box_id", nullable = false, length = 20)
	public String getBoxId() {
		return this.boxId;
	}

	public void setBoxId(String boxId) {
		this.boxId = boxId;
	}

	@Column(name = "customer_article_number", nullable = false)
	public int getCustomerArticleNumber() {
		return this.customerArticleNumber;
	}

	public void setCustomerArticleNumber(int customerArticleNumber) {
		this.customerArticleNumber = customerArticleNumber;
	}

	@Column(name = "customer_lot_number", length = 10)
	public String getCustomerLotNumber() {
		return this.customerLotNumber;
	}

	public void setCustomerLotNumber(String customerLotNumber) {
		this.customerLotNumber = customerLotNumber;
	}

	@Column(name = "test_report", length = 16)
	public String getTestReport() {
		return this.testReport;
	}

	public void setTestReport(String testReport) {
		this.testReport = testReport;
	}

	@Column(name = "certification_year", nullable = false)
	public short getCertificationYear() {
		return this.certificationYear;
	}

	public void setCertificationYear(short certificationYear) {
		this.certificationYear = certificationYear;
	}

	@Column(name = "certification_number_report", length = 15)
	public String getCertificationNumberReport() {
		return this.certificationNumberReport;
	}

	public void setCertificationNumberReport(String certificationNumberReport) {
		this.certificationNumberReport = certificationNumberReport;
	}

	@Column(name = "device_batch_number", length = 20)
	public String getDeviceBatchNumber() {
		return this.deviceBatchNumber;
	}

	public void setDeviceBatchNumber(String deviceBatchNumber) {
		this.deviceBatchNumber = deviceBatchNumber;
	}

	@Column(name = "communication_method", length = 10)
	public String getCommunicationMethod() {
		return this.communicationMethod;
	}

	public void setCommunicationMethod(String communicationMethod) {
		this.communicationMethod = communicationMethod;
	}

	@Column(name = "year_of_manufactory", nullable = false)
	public short getYearOfManufactory() {
		return this.yearOfManufactory;
	}

	public void setYearOfManufactory(short yearOfManufactory) {
		this.yearOfManufactory = yearOfManufactory;
	}

	@Column(name = "hardware_version", length = 50)
	public String getHardwareVersion() {
		return this.hardwareVersion;
	}

	public void setHardwareVersion(String hardwareVersion) {
		this.hardwareVersion = hardwareVersion;
	}

	@Column(name = "module_active_firmware_version", length = 50)
	public String getModuleActiveFirmwareVersion() {
		return this.moduleActiveFirmwareVersion;
	}

	public void setModuleActiveFirmwareVersion(String moduleActiveFirmwareVersion) {
		this.moduleActiveFirmwareVersion = moduleActiveFirmwareVersion;
	}

	@Column(name = "module_target_firmware__version", length = 50)
	public String getModuleTargetFirmwareVersion() {
		return this.moduleTargetFirmwareVersion;
	}

	public void setModuleTargetFirmwareVersion(String moduleTargetFirmwareVersion) {
		this.moduleTargetFirmwareVersion = moduleTargetFirmwareVersion;
	}

	@Column(name = "core_active_firmware_version", length = 30)
	public String getCoreActiveFirmwareVersion() {
		return this.coreActiveFirmwareVersion;
	}

	public void setCoreActiveFirmwareVersion(String coreActiveFirmwareVersion) {
		this.coreActiveFirmwareVersion = coreActiveFirmwareVersion;
	}

	@Column(name = "auxiliary_module_active_firmware_version", length = 50)
	public String getAuxiliaryModuleActiveFirmwareVersion() {
		return this.auxiliaryModuleActiveFirmwareVersion;
	}

	public void setAuxiliaryModuleActiveFirmwareVersion(String auxiliaryModuleActiveFirmwareVersion) {
		this.auxiliaryModuleActiveFirmwareVersion = auxiliaryModuleActiveFirmwareVersion;
	}

	@Column(name = "life_cycle_status", nullable = false, length = 30)
	public String getLifeCycleStatus() {
		return this.lifeCycleStatus;
	}

	public void setLifeCycleStatus(String lifeCycleStatus) {
		this.lifeCycleStatus = lifeCycleStatus;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "start_of_battery_life_date", length = 13)
	public Date getStartOfBatteryLifeDate() {
		return this.startOfBatteryLifeDate;
	}

	public void setStartOfBatteryLifeDate(Date startOfBatteryLifeDate) {
		this.startOfBatteryLifeDate = startOfBatteryLifeDate;
	}

	@Column(name = "battery_type", length = 20)
	public String getBatteryType() {
		return this.batteryType;
	}

	public void setBatteryType(String batteryType) {
		this.batteryType = batteryType;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "battery_replacement_date", length = 13)
	public Date getBatteryReplacementDate() {
		return this.batteryReplacementDate;
	}

	public void setBatteryReplacementDate(Date batteryReplacementDate) {
		this.batteryReplacementDate = batteryReplacementDate;
	}

	@Column(name = "communication_provider", length = 10)
	public String getCommunicationProvider() {
		return this.communicationProvider;
	}

	public void setCommunicationProvider(String communicationProvider) {
		this.communicationProvider = communicationProvider;
	}

	@Column(name = "communication_provider_batch_id", length = 20)
	public String getCommunicationProviderBatchId() {
		return this.communicationProviderBatchId;
	}

	public void setCommunicationProviderBatchId(String communicationProviderBatchId) {
		this.communicationProviderBatchId = communicationProviderBatchId;
	}

	@Column(name = "icc_id", length = 20)
	public String getIccId() {
		return this.iccId;
	}

	public void setIccId(String iccId) {
		this.iccId = iccId;
	}

	@Column(name = "imei", length = 15)
	public String getImei() {
		return this.imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	@Column(name = "communication_module_id", length = 30)
	public String getCommunicationModuleId() {
		return this.communicationModuleId;
	}

	public void setCommunicationModuleId(String communicationModuleId) {
		this.communicationModuleId = communicationModuleId;
	}

	@Column(name = "ppp_authentication_user1", length = 17)
	public String getPppAuthenticationUser1() {
		return this.pppAuthenticationUser1;
	}

	public void setPppAuthenticationUser1(String pppAuthenticationUser1) {
		this.pppAuthenticationUser1 = pppAuthenticationUser1;
	}

	@Column(name = "ppp_authentication_user2", length = 17)
	public String getPppAuthenticationUser2() {
		return this.pppAuthenticationUser2;
	}

	public void setPppAuthenticationUser2(String pppAuthenticationUser2) {
		this.pppAuthenticationUser2 = pppAuthenticationUser2;
	}

	@Column(name = "ppp_authentication_password", length = 15)
	public String getPppAuthenticationPassword() {
		return this.pppAuthenticationPassword;
	}

	public void setPppAuthenticationPassword(String pppAuthenticationPassword) {
		this.pppAuthenticationPassword = pppAuthenticationPassword;
	}

	@Column(name = "modem_hardware_version", length = 50)
	public String getModemHardwareVersion() {
		return this.modemHardwareVersion;
	}

	public void setModemHardwareVersion(String modemHardwareVersion) {
		this.modemHardwareVersion = modemHardwareVersion;
	}

	@Column(name = "modem_firmware_version", length = 50)
	public String getModemFirmwareVersion() {
		return this.modemFirmwareVersion;
	}

	public void setModemFirmwareVersion(String modemFirmwareVersion) {
		this.modemFirmwareVersion = modemFirmwareVersion;
	}

	@Column(name = "equipment_identifier", length = 16)
	public String getEquipmentIdentifier() {
		return this.equipmentIdentifier;
	}

	public void setEquipmentIdentifier(String equipmentIdentifier) {
		this.equipmentIdentifier = equipmentIdentifier;
	}

	@Column(name = "hls_active")
	public Short getHlsActive() {
		return this.hlsActive;
	}

	public void setHlsActive(Short hlsActive) {
		this.hlsActive = hlsActive;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "meter")
	public Set<IMSI> getIMSIs() {
		return this.imsis;
	}

	public void setIMSIs(Set<IMSI> IMSIs) {
		this.imsis = IMSIs;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "meter")
	public Set<Mbus> getMbuses() {
		return this.mbuses;
	}

	public void setMbuses(Set<Mbus> mbuses) {
		this.mbuses = mbuses;
	}

}
