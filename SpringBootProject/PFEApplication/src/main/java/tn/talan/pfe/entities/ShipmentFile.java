package tn.talan.pfe.entities;
// Generated 10 avr. 2019 16:00:40 by Hibernate Tools 5.2.10.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "shipmentfile", schema = "public")
public class ShipmentFile implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private String shipmentFileName;
	private String company;
	private String supplier;
	private String deliveryNoteNumber;
	private String deviceType;
	private String configurationVersion;
	private Date shipmentDate;
	private String orderId;
	private String orderPositionId;
	private Set<Meter> meters = new HashSet<Meter>(0);

	@Id
	@Column(name = "shipment_file_name", unique = true, nullable = false, length = 100)
	public String getShipmentFileName() {
		return this.shipmentFileName;
	}

	public void setShipmentFileName(String shipmentFileName) {
		this.shipmentFileName = shipmentFileName;
	}

	@Column(name = "company", length = 6)
	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Column(name = "supplier", length = 30)
	public String getSupplier() {
		return this.supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	@Column(name = "delivery_note_number", length = 10)
	public String getDeliveryNoteNumber() {
		return this.deliveryNoteNumber;
	}

	public void setDeliveryNoteNumber(String deliveryNoteNumber) {
		this.deliveryNoteNumber = deliveryNoteNumber;
	}

	@Column(name = "device_type", length = 20)
	public String getDeviceType() {
		return this.deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name = "configuration_version", length = 30)
	public String getConfigurationVersion() {
		return this.configurationVersion;
	}

	public void setConfigurationVersion(String configurationVersion) {
		this.configurationVersion = configurationVersion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "shipment_date", length = 13)
	public Date getShipmentDate() {
		return this.shipmentDate;
	}

	public void setShipmentDate(Date shipmentDate) {
		this.shipmentDate = shipmentDate;
	}

	@Column(name = "order_id", length = 10)
	public String getOrderId() {
		return this.orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name = "order_position_id", length = 5)
	public String getOrderPositionId() {
		return this.orderPositionId;
	}

	public void setOrderPositionId(String orderPositionId) {
		this.orderPositionId = orderPositionId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shipmentfile")
	public Set<Meter> getMeters() {
		return this.meters;
	}

	public void setMeters(Set<Meter> meters) {
		this.meters = meters;
	}
}