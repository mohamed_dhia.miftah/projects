package tn.talan.pfe.service;

public interface IActivationRequestService {

	public void activateMeter(String serialNumber);

}
