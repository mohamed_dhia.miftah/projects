package tn.talan.pfe.service;

import org.springframework.web.multipart.MultipartFile;

public interface IStorageService {

	public void storeShipmentFile(MultipartFile file) throws Exception;

}
