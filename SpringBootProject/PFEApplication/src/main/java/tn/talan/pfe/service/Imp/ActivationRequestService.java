package tn.talan.pfe.service.Imp;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import tn.talan.pfe.common.GenerateActivationRequest;
import tn.talan.pfe.config.ConnectActiveMq;
import tn.talan.pfe.service.IActivationRequestService;

@Service
@Component
public class ActivationRequestService implements IActivationRequestService {

	GenerateActivationRequest generateActivationRequest = new GenerateActivationRequest();
	ConnectActiveMq connectActiveMq = new ConnectActiveMq();

	public void activateMeter(String serialNumber) {
		
		String activationRequest = generateActivationRequest.generate(serialNumber);
		
		try {
			connectActiveMq.sendActivationRequest(activationRequest);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
