package tn.talan.pfe.service.Imp;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import tn.talan.pfe.XmlDto.install.MeterAssetAddRequestMessage;
import tn.talan.pfe.common.ConvertionClass;
import tn.talan.pfe.dao.MeterRepository;
import tn.talan.pfe.entities.Meter;
import tn.talan.pfe.service.IActivationRequestService;

@Service
@Component
public class InstallReportService {

	@Autowired
	MeterRepository meterRepository;
	@Autowired
	IActivationRequestService iActivationRequestService;

	/* les classes a utilisé */
	ConvertionClass convertionClass = new ConvertionClass();

	@JmsListener(destination = "InstallReport.queue")
	public void receive(String installReport) throws Exception {

		String message = "";

		StringReader installReportFile = new StringReader(installReport);

		// JaxB + Unmarshaller
		JAXBContext context = JAXBContext.newInstance(MeterAssetAddRequestMessage.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		MeterAssetAddRequestMessage meterAssetAddRequestMessage = (MeterAssetAddRequestMessage) unmarshaller
				.unmarshal(installReportFile);

		String mrid = meterAssetAddRequestMessage.getPayload().getMeterAsset().getMRID();
		Meter meter = meterRepository.findById(mrid).get();

		if (meter == null) {
			message = "Meter does not exist !";
			System.out.println(message);
//			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
		} else {
			message = "Meter found !";
			System.out.println(message);

			XMLGregorianCalendar xmlDate = meterAssetAddRequestMessage.getHeader().getDateTime();
			meter.setInstallDateTime(convertionClass.convertxmlGregorianCalendartoDate(xmlDate));

			String lifeCycleStatus = meter.getLifeCycleStatus();

			if (lifeCycleStatus.equals("PROVISIONED")) {
//				meter.setLifeCycleStatus("INSTALLED");
				System.out.println("from PROVISIONED to INSTALLED");

			} else if (lifeCycleStatus.equals("DISCOVERED")) {
//				meter.setLifeCycleStatus("RECONCILED");
				System.out.println("from DISCOVERED to RECONCILED");
				/* save changes */
//				meterRepository.saveAndFlush(meter);

				/* activate Meter */
//				iActivationRequestService.activateMeter(mrid);
			}

			/* save changes */
//			meterRepository.saveAndFlush(meter);
		}
	}
}
