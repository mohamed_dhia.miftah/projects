package tn.talan.pfe.service.Imp;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.talan.pfe.XmlDto.CargoBoxTypeDTO;
import tn.talan.pfe.XmlDto.DeviceAttributesTypeDTO;
import tn.talan.pfe.XmlDto.PalletTypeDTO;
import tn.talan.pfe.XmlDto.ShipmentFileDTO;
import tn.talan.pfe.common.ConvertionClass;
import tn.talan.pfe.dao.MeterRepository;
import tn.talan.pfe.entities.Meter;
import tn.talan.pfe.service.IMeterService;

@Service
public class MeterService implements IMeterService {

	@Autowired
	MeterRepository meterRepository;

	@Override
	public void saveMeter(String xmlFile) {

		Meter meter = new Meter();
//		ShipmentFile shipmentFile = new ShipmentFile();
		ConvertionClass convertionClass = new ConvertionClass();

		try {
			// JaxB + Unmarshaller
			JAXBContext context = JAXBContext.newInstance(ShipmentFileDTO.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			ShipmentFileDTO shipmentFileDTO = (ShipmentFileDTO) unmarshaller.unmarshal(new File(xmlFile));

//			String shipmentFileName = Paths.get(xmlFile).getFileName().toString();
//			meter.setShipmentfile();

			for (PalletTypeDTO palletTypeDTO : shipmentFileDTO.getBody().getShipment().getPallet()) {
				String palletId = palletTypeDTO.getPalletId();
				meter.setPalletId(palletId);
//				System.out.println(palletTypeDTO.getPalletId());

				for (CargoBoxTypeDTO cargoBoxTypeDTO : palletTypeDTO.getBox()) {
					String boxId = cargoBoxTypeDTO.getBoxId();
					meter.setBoxId(boxId);

					for (DeviceAttributesTypeDTO deviceAttributesTypeDTO : cargoBoxTypeDTO.getDeviceAttributes()) {
						String serialNumber = deviceAttributesTypeDTO.getGeneral().getSerialNumber();
						String customerArticleNumber = deviceAttributesTypeDTO.getGeneral().getCustomerArticleNumber();
						String customerLotNumber = deviceAttributesTypeDTO.getGeneral().getCustomerLotNumber();
						String testReport = deviceAttributesTypeDTO.getGeneral().getTestReport();
						String certificationYear = deviceAttributesTypeDTO.getGeneral().getCertificationYear();
						String certificationNumberReport = deviceAttributesTypeDTO.getGeneral()
								.getCertificationNumberReport();
						String deviceBatchNumber = deviceAttributesTypeDTO.getGeneral().getDeviceBatchNumber();
						String communicationMethod = deviceAttributesTypeDTO.getGeneral().getCommunicationMethod();
						String yearOfManufactory = deviceAttributesTypeDTO.getGeneral().getYearOfManufactory();
						String hardwareVersion = deviceAttributesTypeDTO.getGeneral().getHardwareVersion();
						String coreActiveFirmwareVersion = deviceAttributesTypeDTO.getGeneral()
								.getCoreActiveFirmwareVersion();
//						System.out.println(coreActiveFirmwareVersion);
						String equipmentIdentifier = deviceAttributesTypeDTO.getDLMSAttributes()
								.getEquipmentIdentifier();
						String lifeCycleStatus = "PROVISIONED";

						meter.setSerialNumber(serialNumber);
						meter.setCustomerArticleNumber(convertionClass.convertStringToInt(customerArticleNumber));
						meter.setCustomerLotNumber(customerLotNumber);
						meter.setTestReport(testReport);
						meter.setCertificationYear((short) convertionClass.convertStringToInt(certificationYear));
						meter.setCertificationNumberReport(certificationNumberReport);
						meter.setDeviceBatchNumber(deviceBatchNumber);
						meter.setCommunicationMethod(communicationMethod);
						meter.setYearOfManufactory((short) convertionClass.convertStringToInt(yearOfManufactory));
						meter.setHardwareVersion(hardwareVersion);
						meter.setCoreActiveFirmwareVersion(coreActiveFirmwareVersion);
						meter.setEquipmentIdentifier(equipmentIdentifier);
						meter.setLifeCycleStatus(lifeCycleStatus);

						meterRepository.saveAndFlush(meter);
					}

				}

			}

		} catch (JAXBException e) {
//			 TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
