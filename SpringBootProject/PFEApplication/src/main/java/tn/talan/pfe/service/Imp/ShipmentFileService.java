package tn.talan.pfe.service.Imp;

import java.io.File;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.talan.pfe.XmlDto.ShipmentFileDTO;
import tn.talan.pfe.common.ConvertionClass;
import tn.talan.pfe.dao.ShipmentFileRepository;
import tn.talan.pfe.entities.ShipmentFile;
import tn.talan.pfe.service.IShipmentFileService;

@Service
public class ShipmentFileService implements IShipmentFileService {

	@Autowired
	ShipmentFileRepository shipmentFileRepository;

	@Override
	public void saveShipmentFile(String xmlFile) {

		ShipmentFile shipmentFile = new ShipmentFile();

		try {
			// JaxB + Unmarshaller
			JAXBContext context = JAXBContext.newInstance(ShipmentFileDTO.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			ShipmentFileDTO shipmentFileDTO = (ShipmentFileDTO) unmarshaller.unmarshal(new File(xmlFile));

			String shipmentFileName = Paths.get(xmlFile).getFileName().toString();
//			System.out.println(Paths.get(xmlFile).getFileName());
			String company = shipmentFileDTO.getBody().getShipment().getMain().getCompany();
			String supplier = shipmentFileDTO.getBody().getShipment().getMain().getSupplier();
			String deliveryNoteNumber = shipmentFileDTO.getBody().getShipment().getHeader().getDeliveryNoteNumber();
			String deviceType = shipmentFileDTO.getBody().getShipment().getHeader().getDeviceType();
			String configurationVersion = shipmentFileDTO.getBody().getShipment().getHeader().getConfigurationVersion();
			String shipDate = shipmentFileDTO.getBody().getShipment().getHeader().getShipmentDate();
			String orderId = shipmentFileDTO.getBody().getShipment().getHeader().getOrderId();
			String orderPositionId = shipmentFileDTO.getBody().getShipment().getHeader().getOrderPositionId();

			// convertir shipmentDate from string to date
			ConvertionClass convertionClass = new ConvertionClass();
			Date shipmentDate = convertionClass.convertStringToDate(shipDate);

//			String dateString = shipDate.substring(6, 7) + "/" + shipDate.substring(4, 6) + "/"
//					+ shipDate.substring(0, 4);
//			Date shipmentDate = new SimpleDateFormat("dd/MM/yyyy").parse(dateString);
//			System.out.println(shipmentDate);

			shipmentFile.setShipmentFileName(shipmentFileName);
			shipmentFile.setCompany(company);
			shipmentFile.setSupplier(supplier);
			shipmentFile.setDeliveryNoteNumber(deliveryNoteNumber);
			shipmentFile.setDeviceType(deviceType);
			shipmentFile.setConfigurationVersion(configurationVersion);
			shipmentFile.setShipmentDate(shipmentDate);
			shipmentFile.setOrderId(orderId);
			shipmentFile.setOrderPositionId(orderPositionId);

//			shipmentFileRepository.saveAndFlush(shipmentFile);

		} catch (JAXBException e) {
//			 TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println("erreur en convertion");
			e.printStackTrace();
		}
	}
}
