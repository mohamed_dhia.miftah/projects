package tn.talan.pfe.service.Imp;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import tn.talan.pfe.XmlDto.CargoBoxTypeDTO;
import tn.talan.pfe.XmlDto.DeviceAttributesTypeDTO;
import tn.talan.pfe.XmlDto.PalletTypeDTO;
import tn.talan.pfe.XmlDto.ShipmentFileDTO;
import tn.talan.pfe.common.ConvertionClass;
import tn.talan.pfe.common.XMLValidation;
import tn.talan.pfe.dao.MeterRepository;
import tn.talan.pfe.dao.ShipmentFileRepository;
import tn.talan.pfe.entities.Meter;
import tn.talan.pfe.entities.ShipmentFile;
import tn.talan.pfe.service.IStorageService;

@Service
public class StorageService implements IStorageService {

//	@Autowired
//	IShipmentFileService iShipmentFileService;
//	@Autowired
//	IMeterService iMeterService;

	@Autowired
	MeterRepository meterRepository;
	@Autowired
	ShipmentFileRepository shipmentFileRepository;

	private final Path rootLocation = Paths.get("uploadDirectory");
	private final String XSDFILE = "src/main/resources/xsdResources/Fluvius_SF_gas_v1.5.xsd";

	public void storeShipmentFile(MultipartFile file) throws Exception {

		String xmlFileLocation = rootLocation + "/" + file.getOriginalFilename();

		try {
			Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()));
		} catch (Exception e) {
			throw new RuntimeException("FAIL!");
		}

		/* Validate xml File with his xsd File */
		XMLValidation xmlValidation = new XMLValidation();
		Boolean validationResult = xmlValidation.validation(xmlFileLocation, XSDFILE);

		if (validationResult == true) {
			StoreDataBase(xmlFileLocation);
		} else {
//			File xmlFile = new File(xmlFileLocation);
//			xmlFile.delete();
//			System.out.println("deleted");
			FileSystemUtils.deleteRecursively(Paths.get(xmlFileLocation));
			System.out.println("deleted");
		}

	}

	public void StoreDataBase(String xmlFile) {

//		iShipmentFileService.saveShipmentFile(xmlFile);
//		iMeterService.saveMeter(xmlFile);

		ShipmentFile shipmentFile = new ShipmentFile();
		Meter meter = new Meter();

		try {
			// JaxB + Unmarshaller
			JAXBContext context = JAXBContext.newInstance(ShipmentFileDTO.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			ShipmentFileDTO shipmentFileDTO = (ShipmentFileDTO) unmarshaller.unmarshal(new File(xmlFile));

			/* Extract shipmentFile data from XML File */
			String shipmentFileName = Paths.get(xmlFile).getFileName().toString();
			String company = shipmentFileDTO.getBody().getShipment().getMain().getCompany();
			String supplier = shipmentFileDTO.getBody().getShipment().getMain().getSupplier();
			String deliveryNoteNumber = shipmentFileDTO.getBody().getShipment().getHeader().getDeliveryNoteNumber();
			String deviceType = shipmentFileDTO.getBody().getShipment().getHeader().getDeviceType();
			String configurationVersion = shipmentFileDTO.getBody().getShipment().getHeader().getConfigurationVersion();
			String shipDate = shipmentFileDTO.getBody().getShipment().getHeader().getShipmentDate();
			String orderId = shipmentFileDTO.getBody().getShipment().getHeader().getOrderId();
			String orderPositionId = shipmentFileDTO.getBody().getShipment().getHeader().getOrderPositionId();

			/* convert shipmentDate from string to date */
			ConvertionClass convertionClass = new ConvertionClass();
			Date shipmentDate = convertionClass.convertStringToDate(shipDate);

			/* Set ShipmentFile data to the entity */
			shipmentFile.setShipmentFileName(shipmentFileName);
			shipmentFile.setCompany(company);
			shipmentFile.setSupplier(supplier);
			shipmentFile.setDeliveryNoteNumber(deliveryNoteNumber);
			shipmentFile.setDeviceType(deviceType);
			shipmentFile.setConfigurationVersion(configurationVersion);
			shipmentFile.setShipmentDate(shipmentDate);
			shipmentFile.setOrderId(orderId);
			shipmentFile.setOrderPositionId(orderPositionId);

			/* Save ShipmentFile entity Data */
			shipmentFileRepository.saveAndFlush(shipmentFile);

			/* Extract Meter data from XML File */
			for (PalletTypeDTO palletTypeDTO : shipmentFileDTO.getBody().getShipment().getPallet()) {
				String palletId = palletTypeDTO.getPalletId();
				meter.setPalletId(palletId);

				for (CargoBoxTypeDTO cargoBoxTypeDTO : palletTypeDTO.getBox()) {
					String boxId = cargoBoxTypeDTO.getBoxId();
					meter.setBoxId(boxId);

					for (DeviceAttributesTypeDTO deviceAttributesTypeDTO : cargoBoxTypeDTO.getDeviceAttributes()) {
						String serialNumber = deviceAttributesTypeDTO.getGeneral().getSerialNumber();
						String customerArticleNumber = deviceAttributesTypeDTO.getGeneral().getCustomerArticleNumber();
						String customerLotNumber = deviceAttributesTypeDTO.getGeneral().getCustomerLotNumber();
						String testReport = deviceAttributesTypeDTO.getGeneral().getTestReport();
						String certificationYear = deviceAttributesTypeDTO.getGeneral().getCertificationYear();
						String certificationNumberReport = deviceAttributesTypeDTO.getGeneral()
								.getCertificationNumberReport();
						String deviceBatchNumber = deviceAttributesTypeDTO.getGeneral().getDeviceBatchNumber();
						String communicationMethod = deviceAttributesTypeDTO.getGeneral().getCommunicationMethod();
						String yearOfManufactory = deviceAttributesTypeDTO.getGeneral().getYearOfManufactory();
						String hardwareVersion = deviceAttributesTypeDTO.getGeneral().getHardwareVersion();
						String coreActiveFirmwareVersion = deviceAttributesTypeDTO.getGeneral()
								.getCoreActiveFirmwareVersion();
						String equipmentIdentifier = deviceAttributesTypeDTO.getDLMSAttributes()
								.getEquipmentIdentifier();
						String lifeCycleStatus = "PROVISIONED";

						/* Set Meter data to the entity */
						meter.setSerialNumber(serialNumber);
						meter.setCustomerArticleNumber(convertionClass.convertStringToInt(customerArticleNumber));
						meter.setCustomerLotNumber(customerLotNumber);
						meter.setTestReport(testReport);
						meter.setCertificationYear((short) convertionClass.convertStringToInt(certificationYear));
						meter.setCertificationNumberReport(certificationNumberReport);
						meter.setDeviceBatchNumber(deviceBatchNumber);
						meter.setCommunicationMethod(communicationMethod);
						meter.setYearOfManufactory((short) convertionClass.convertStringToInt(yearOfManufactory));
						meter.setHardwareVersion(hardwareVersion);
						meter.setCoreActiveFirmwareVersion(coreActiveFirmwareVersion);
						meter.setEquipmentIdentifier(equipmentIdentifier);
						meter.setLifeCycleStatus(lifeCycleStatus);
						meter.setShipmentfile(shipmentFile);

						/* Save Meter entity Data */
						meterRepository.saveAndFlush(meter);

						/* Extract M-Bus data from XML File */
//						for (CommunicationIdGasTypeDTO communicationIdGasTypeDTO : deviceAttributesTypeDTO.getMbus()
//								.getCommunicationIdGas()) {
//
//						}
					}
				}
			}

		} catch (JAXBException e) {
//			 TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println("erreur de convertion");
			e.printStackTrace();
		}
	}
}
