package com.dhia.employeeManagerApp.exception;

public class userNotFoundException extends RuntimeException{
    public userNotFoundException(String message) {
        super(message);
    }
}
