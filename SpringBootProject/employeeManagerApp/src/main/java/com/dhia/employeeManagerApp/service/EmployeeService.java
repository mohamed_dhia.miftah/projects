package com.dhia.employeeManagerApp.service;

import com.dhia.employeeManagerApp.exception.userNotFoundException;
import com.dhia.employeeManagerApp.model.Employee;
import com.dhia.employeeManagerApp.repo.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class EmployeeService {
    private final EmployeeRepo employeeRepo;

    @Autowired
    public EmployeeService(EmployeeRepo employeeRepo) {
        this.employeeRepo = employeeRepo;
    }

    public Employee addEmployee(Employee employee) {
        employee.setEmployeeCode(UUID.randomUUID().toString());
        return employeeRepo.save(employee);
    }

    public List<Employee> getAllEmployees() {
        return employeeRepo.findAll();
    }

    public Employee updateEmployee(Employee employee) {
        employeeRepo.findById(employee.getId());
        return employeeRepo.save(employee);
    }

    public Employee findEmployeeById(Long id) {
        return employeeRepo.findEmployeeById(id).orElseThrow(() -> new userNotFoundException("User by id " + id + " was not found !"));
    }

    public void deleteEmployeeById(Long id) {
        // employeeRepo.deleteById(id);
        employeeRepo.deleteEmployeeById(id);
    }
}
