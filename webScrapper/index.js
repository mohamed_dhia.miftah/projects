const PORT = 8000;
const URL = "www.mytek.tn";
const axios = require('axios');
const cheerio = require('cheerio');
const express = require('express');
const {response} = require("express");

const app = express();

axios(URL)
    .then(response => {
        const htmlResponse = response.data
        console.log(htmlResponse)
        const $ = cheerio.load(htmlResponse);
        const articleArray = []
        $('.product name product-item-name',htmlResponse).each(function (){
            const titleObject = $(this).text()
            const urlObject = $(this).find('a').attr('href')
            articleArray.push({
                article : titleObject,
                url: urlObject
            })
        })
        console.log("articles = ", articleArray)
    }).catch(err => console.log(err))

app.listen(PORT ,() => console.log(`server running in PORT ${PORT}`) )